#include"coro-manager.h"
namespace ns3{
    NS_LOG_COMPONENT_DEFINE("CoroManager");
    NS_OBJECT_ENSURE_REGISTERED(CoroManager);


    TypeId CoroManager::GetTypeId(void)
    {
        static TypeId tid = TypeId("ns3::CoroManager")
            .SetParent<Object>()
            .SetGroupName("MyManager")
            .AddConstructor<CoroManager>();

        return tid;
    }
    void CoroManager::GetTaskInfo(std::initializer_list<std::string> taskInfo)
    {
        m_generateUnit->GetInfo(taskInfo);
        Ptr<Task> task = m_generateUnit->Generate();
    }
    void CoroManager::Start(void)
    {
        RunTask();
        //SendTask();
    }

    void CoroManager::GenerateTask(Ptr<Task> t)
    {
        m_taskStatusTable->GetTask(t);
    }
    bool CoroManager::ReceiveTask(Ptr<Task> t)
    {
        //先判断t任务的执行时间，如果小于当前时间。。。。就丢掉？
//        auto success =  m_receiveUnit->ReceiveTask(t);
//        Ptr<Task> task = t;
////      std::cout<<1<<'\t';
//        Time dt = task->GetGenerateTime();
//        Ptr<Task> temp = m_taskStatusTable->FirstPriorInPending();
////        while(dt>=temp->GetWaitTime())
////        {
////            m_taskStatusTable->DeletePendingFirst();
////            static Ptr<Task> temp = m_taskStatusTable->FirstPriorInPending();
////        }
//        if(dt>=temp->GetGenerateTime())
//        {
//        Time endTime = task->GetEndTime();
//
//        auto it = find_if(m_taskRelease.begin(),m_taskRelease.end(),[task](Ptr<Task> t){return  task->GetEndTime()<=t->GetEndTime();});
//        m_taskRelease.insert(it,task);
//        Simulator::Schedule(dt-Now(),&CoroManager::RunTask,this);
//        if(endTime<=Seconds(500))
//        {
//            Simulator::Schedule(endTime-Now(),&CoroManager::Release,this);
//
//        }
//        }
        //因为目前缺乏任务的发送时间，只有任务的调度时间。所以暂时先在时间上加个1,来抵消掉packet在信道上传输的时延。
        Time dt = t->GetGenerateTime();
        Time now = Now();
            std::cout<<"--------------------------------"<<std::endl;
            std::cout<<dt<<'\t'<<now<<std::endl;
            std::cout<<"--------------------------------"<<std::endl;
        //manager在reveive之前先对任务的创建时间进行判断。假如创建时间小于当前时间，说明任务来得晚了，可能因为节点故障也可能因为链路拥堵。那么此时任务的执行策略是否应该发生变化？应该如何变化？（任务的优先级动态变化？任务再次进行转发？或者传输过程中对任务进行备份，超过一定时间就在备份节点上直接执行？）（这应该是可靠性方面的问题了吧）
        if(dt<now)
        {
            t->SetGenerateTime(0);
            m_receiveUnit->ReceiveFromOthers(t);
            Time endTime =t->GetEndTime();
            auto it = find_if(m_remoteRelease.begin(),m_remoteRelease.end(),[t](Ptr<Task> task){return  t->GetEndTime()<=task->GetEndTime();});
            m_remoteRelease.insert(it,t);
          //Simulator::Schedule(Now(),&CoroManager::RunReceivedTask,this);
            Simulator::ScheduleNow(&CoroManager::RunReceivedTask,this);
            Simulator::Schedule(t->GetEndTime()-Now(),&CoroManager::RemoteRelease,this);
            return false;
        }
        else
        {
        //这里m_receiveUnit->ReceiveTask()到的task应该单独存放起来，与本地调度的任务区分开来。现在先receive一个就立即Schedule一个。
            m_receiveUnit->ReceiveFromOthers(t);
//            Ptr<Task> task = m_taskStatusTable->FirstPriorInPending();
//            m_taskStatusTable->DeletePendingFirst();
            Time endTime =t->GetEndTime();
            auto it = find_if(m_taskRelease.begin(),m_taskRelease.end(),[t](Ptr<Task> task){return  t->GetEndTime()<=task->GetEndTime();});
            m_taskRelease.insert(it,t);
            Simulator::Schedule(Seconds(0),&CoroManager::RunReceivedTask,this);
            Simulator::Schedule(t->GetEndTime()-Now(),&CoroManager::RemoteRelease,this);
            return true;
        }

    }
    void CoroManager::SendTask(Ptr<Task> t,Time dt)
    {
        //默认client安装在0,server安装在1
        const Task task = *t;
        auto client = DynamicCast<Client>(m_node->GetApplication(0));
        client->ReceiveTask(task,dt);

    }
    void CoroManager::RunTask(void)
    {
        m_orchestratorUnit->ChooseTask();
   //     m_orchestratorUnit->Run();
    }
    void CoroManager::SendTask(void)
    {
        m_sendUnit->Start();
    }
    void CoroManager::RunSingleTask(void)
    {
       // m_orchestratorUnit->Run();
    }
    void CoroManager::RunReceivedTask(void)
    {
        m_orchestratorUnit->ChooseReceivedTask();
        m_orchestratorUnit->RunReceivedTask();
    }

    void CoroManager::ShowTaskStatus(void)
    {
        m_taskStatusTable->Print();
    }
    void CoroManager::Release()
    {
        Time t = Simulator::Now();
        auto it = find_if(m_taskRelease.begin(),m_taskRelease.end(),[t](Ptr<Task> task){return  t<=task->GetEndTime();});
	    auto task = *(it);
        m_taskRelease.erase(it);
	    m_orchestratorUnit->ReleaseResource(task,0);

    }
    void CoroManager::RemoteRelease()
    {
        Time t = Simulator::Now();
        auto it = find_if(m_remoteRelease.begin(),m_remoteRelease.end(),[t](Ptr<Task> task){return  t<=task->GetEndTime();});
	    auto task = *(it);
        m_remoteRelease.erase(it);
	    m_orchestratorUnit->ReleaseResource(task,1);

    }
    void CoroManager::SetNode(Ptr<Node> node)
    {
        m_node = node;
        m_orchestratorUnit = Create<RRMachine>(m_taskStatusTable,m_receiveTable,m_sendTable,m_node,m_machineId);
        m_generateUnit = Create<DefaultGenerate>(m_taskStatusTable,m_sendTable,m_machineId,m_node);
        m_receiveUnit = Create<DefaultReceive>(m_taskStatusTable,m_receiveTable,m_machineId);
        m_sendUnit = Create<DefaultSend>(m_sendTable,m_machineId,m_node);
    }
    Ptr<Node> CoroManager::GetNode(void) const
    {
        return m_node;
    }
    void CoroManager::SetMachineId(std::string Id)
    {
        m_machineId = Id;//还没改完，部件内的Id也要改
        m_generateUnit->SetMachineId(Id);
        m_receiveUnit->SetMachineId(Id);
        m_orchestratorUnit->SetMachineId(Id);
    }
    void CoroManager::SetMap(std::map<std::string,Address> DNS)
    {
        m_sendUnit->SetMap(DNS);  //DNS功能转入application，sendUnit把该功能删除
        //auto client = DynamicCast<Client>(m_node->GetApplication(0));
        //client->SetMap(DNS);
    }
    void CoroManager::SetSETime(double start,double end)
    {
        m_sendUnit->SetClientTime(start,end);
    }

}
