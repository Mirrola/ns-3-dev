#include "ns3/dcoro-task.h"
namespace ns3{
    NS_LOG_COMPONENT_DEFINE("DCoroTask");
    NS_OBJECT_ENSURE_REGISTERED(DCoroTask);
    
    void DCoroTask::StateInit(){
        //反序列化的调用也要改一改
        ObjectFactory fac;
       // fac.SetTypeId("ns3::Example1");
        fac.SetTypeId(m_exTypeId);
        Ptr<Example1> example = fac.Create<Example1>();
        Ptr<Example1::State> state = Create<Example1::State>();
        //Ptr<Example1::State> state = (*example)::State;
        m_funcChain.UpdateState(state);
        example->InitTask(*this);
    }

    Time DCoroTask::Continue(){
        //记录实际执行时间并返回 ms
        auto start = std::chrono::system_clock::now();
        //do something
        m_funcChain.Continue();
        auto end = std::chrono::system_clock::now();
        auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
        std::cout<<elapsed.count()<<" ms"<<std::endl;
        return MilliSeconds(elapsed.count());
//        typedef std::chrono::high_resolution_clock Clock;
//        auto t1 = Clock::now();//计时开始
//        m_funcChain.Continue();
//        auto t2 = Clock::now();//计时结束
//        double nm = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count();
//        std::cout <<nm << ": nm !!!!!!!!"<< '\n';
//        return MilliSeconds(nm);
    }

    void DCoroTask::AddFunc(std::function<DefaultCoro(Ptr<StateBase>)> func){
        m_funcChain.AddFunc(func);
    }

    bool DCoroTask::Done(){
        return m_funcChain.Done();
    }

}
