/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
#ifndef DCOROTASK_H
#define DCOROTASK_H
#include <boost/serialization/serialization.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/base_object.hpp>
#include <list>
#include <vector>
#include <boost/serialization/list.hpp>
#include"ns3/task.h"
#include"ns3/list-chain.h"
#include<functional>
#include <chrono>
#include"ns3/example-1.h"
namespace ns3 {
    class DCoroTask : public Task{
        friend class boost::serialization::access;
        public:
//            DCoroTask(double requestcpu,double requestmem ,std::vector<std::function<DefaultCoro(Ptr<StateBase>)>> vec, Ptr<StateBase> state):Task(requestcpu,requestmem){
//                StateInit(state);
//                for(auto it : vec){
//                    AddFunc(it);
//                }
//            }
            DCoroTask(double requestcpu, double requestmem) : Task(requestcpu, requestmem){}
            DCoroTask(double requestcpu, double requestmem, std::string typeId) : Task(requestcpu, requestmem), m_exTypeId(typeId){}

            DCoroTask() =default;
            virtual ~DCoroTask(){};
            
            void StateInit();

            Time Continue();
            
            void SetExample(const std::string& tId){
                m_exTypeId = tId;
            }

            bool Done();
//            template<class Ar> void serialize(Ar &ar,const unsigned int version)
//            {
//                ar.template register_type<Task>();
//                ar & boost::serialization::base_object<Task>(*this);
//                ar & m_machine_list;
//                ar & m_time;
//                ar & sendTest;
//                ar & receiveTest;
//            }
            /*
             * 序列化这部分没有问题，对一个中间变量进了序列化操作，反序列化呢？
             * 其实可以优化：没有必要构建一个Example中间变量。直接对m_exTypeId序列化
             * 及反序列化即可，只不过反序列化在初始之前需要构造好m_funcChain对象！！
             * */
            template<class Ar> void serialize(Ar &ar, const unsigned int version){
                ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Task);
                ar & m_exTypeId;
                //ar & boost::serialization::base_object<Task>(*this);
            }
            
            void AddFunc(std::function<DefaultCoro(Ptr<StateBase>)>);
        private:

            // m_funcChain包含子任务间的执行关系
            ListChain m_funcChain;
            
            // m_exTypeId保存Example的typeid信息，用于序列化和反序列化。
            std::string m_exTypeId;

            
    };
}
#endif /* DCOROTASK_H */

