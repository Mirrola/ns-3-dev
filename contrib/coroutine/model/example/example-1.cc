#include "ns3/example-1.h"
namespace ns3{
  
    NS_LOG_COMPONENT_DEFINE("Example1");
    NS_OBJECT_ENSURE_REGISTERED(Example1);

    TypeId Example1::GetTypeId(void){
        static TypeId tid = TypeId("ns3::Example1")
            .SetParent<Object>()
            .SetGroupName("CoroExample")
            .AddConstructor<Example1>();
        return tid;
    }

    void Example1::State::StateTransfer(Ptr<StateBase> s){
        State* state = dynamic_cast<State*>(GetPointer(s));
        Example1::State::a = state->a;
        Example1::State::b = state->b;
    }

    void Example1::InitTask(DCoroTask &task){
       //Ptr<State> state = Create<State>();
       //task.StateInit(state);
       task.AddFunc(func1);
       task.AddFunc(func2);
       task.AddFunc(func3);
    }
    
    DefaultCoro Example1::func1(Ptr<StateBase> s){
        for(int i = 1; i <= 100; ++i){
            if(i == 50){
                co_await std::suspend_always{};
            }
            State* state = dynamic_cast<State*>(GetPointer(s));
            state->a++;
        }
    }
    DefaultCoro Example1::func2(Ptr<StateBase> s){
        for(int i = 1; i <= 1000 ; ++i){
            if(i == 50){
                co_await std::suspend_always{};
            }
            State* state = dynamic_cast<State*>(GetPointer(s));
            state->b++;
        }
    }
    DefaultCoro Example1::func3(Ptr<StateBase> s){
        State* state = dynamic_cast<State*>(GetPointer(s));
        state->res = state->a - state->b;
        co_return state;
    }
}

