#ifndef CHAINBASE_H
#define CHAINBASE_H
#include"ns3/object.h"
#include"ns3/core-module.h"
#include"ns3/ptr.h"
#include<list>
namespace ns3{
    /*
     * 中间的同步全由协程的init_suspend和final_suspend完成，
     * 因此这里不需要留出同步的操作。
     *
     * 但是此处需要记录同步的总信息，要一直和子协程更新信息。
     * 这里同步是需要对任务信息记录，所以保存一个Ptr<Task>,
     * 并且更新的接口也留作Ptr<Task>
     *
     * 那么chain和Task的关系是什么，毕竟调度的时候需要传入一个task对象
     * 所以chain是task的一个元素，作为task的友元
     * */
    class ChainBase : public Object{
        public:
            virtual bool Continue() = 0;
            //vtual bool Record(Task *) = 0; 这一部放在state machine来做吧
    };
}
#endif /* CHAINBASE_H */
