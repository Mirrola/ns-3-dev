#include"ns3/rr-machine.h"
#include"ns3/log.h"
namespace ns3{
    NS_LOG_COMPONENT_DEFINE("RRMachine");
    NS_OBJECT_ENSURE_REGISTERED(RRMachine);

    /*--------------------------public--------------------------*/
    bool RRMachine::ChooseTask()
    {
        if(m_taskStatusTable->PendingEmpty())
        {
            std::cerr<<" has no pending task to run. "<<std::endl;
            return 0;
        }
        OneRoundDispatch();
        //Simulator::Schedule (Simulator::Now (), &RRMachine::OneRoundDispatch, this);
        return 1;
    }
    void RRMachine::Transit (Time dt)
    {
        NS_LOG_FUNCTION (this << dt);
        // auto pendingList = m_taskStatusTable->GetPendingList ();
        // auto runningList = m_taskStatusTable->GetRunningList ();
        Simulator::Schedule (dt, &RRMachine::OneRoundDispatch, this);
    }
    void RRMachine::OneRoundDispatch ()
    {
        Time time = Simulator::Now ();
        NS_LOG_INFO (Simulator::Now ());
        auto& pendingList = m_taskStatusTable->GetPendingList ();
        //auto& runningList = m_taskStatusTable->GetRunningList ();
        if(pendingList.empty() != true){
            Ptr<Task> it = pendingList.front();
            //pendingList.pop_front();
            if (it->GetGenerateTime () <= time)
            {
                std::cout<<"运行！"<<std::endl;
                Run(it);
            }
            else 
            {
                pendingList.push_back(it);
            }
        }
        //std::list<Ptr<Task>>::iterator it = pendingList.begin ();
        Transit(m_freq);
    }
    bool RRMachine::ChooseReceivedTask()
    {
//		if(m_receiveTable->PendingEmpty())
//		{
//		    std::cerr<<" has no pending task to run. "<<std::endl;
//		    return 0;
//		}
//        auto list = m_receiveTable->GetPendingList();
//        m_task = list.front();
////        m_task = *list.begin();
////        for(auto it = list.begin();it!=list.end();++it)
////        {
////				    m_task = *it;
////            Simulator::Schedule(m_task->GetGenerateTime(),&RRMachine::RunReceivedTask,this);
////        }
//	    return 1;
        return true;
	}
    void RRMachine::StataTransfer (Ptr<Task> t, int i)
    {
      if (i == 0)
      {
          m_taskStatusTable->PendingToRunning (t);
      }
      else if (i == 1)
      {
          m_taskStatusTable->RunningToDead (t);
      }
      else
      {
          m_taskStatusTable->RunningToPending (t);
      }
    }
    void RRMachine::ReceivedStataTransfer (Ptr<Task> t, int i)
    {
//      if (i == 0)
//      {
//          m_receiveTable->PendingToRunning (t);
//      }
//      else if (i == 1)
//      {
//          m_receiveTable->RunningToDead (t);
//      }
//      else
//      {
//          m_receiveTable->RunningToPending (t);
//      }
        ;
    }
    void RRMachine::Run(Ptr<Task> task)
    {
        NS_LOG_FUNCTION(this);
		if(!m_taskStatusTable->CheckIn(task))
        {
            return;
        }
		Ptr<ResourceContainer> container = m_node->GetObject<ResourceContainer>();
		for(auto it = container->Begin();it!=container->End();++it)
		{
		    Ptr<Resource> resource = *it;
            NS_LOG_INFO ("EasiEI:  At time " << Simulator::Now ().As (Time::S) << " Machine: " <<GetMachineId()<<'\t'<< "  Running task  "<<task->GetTId() << std::endl);
		    if(resource->Allocate(task->GetRCpu(),task->GetRMemory())==true)
		    {
		        StataTransfer(task,0);
		        m_taskAllocateTable.insert(std::make_pair(task->GetTId(),resource->GetResourceId()));
                DCoroTask *temp = dynamic_cast<DCoroTask*>(GetPointer(task));
                //Ptr<DCoroTask> coroTask = Ptr<DCoroTask>(temp);
                while(!temp->Done()){
                    //记录任务continue的执行时间
                    m_slice -= temp->Continue();
                    //时间片的同步应该封装在continue的内部完成！
                    //task->Continue(); //执行协程链
                    //同步时间：若时间片用完则停止，并将任务push回原队列。若未用完且任务未完成则继续run
                    //
                    //时间同步要补上
                    if(m_slice >= MilliSeconds(0) && !temp->Done()){
                        continue;
                    }
                    else if(temp->Done()){
                        StataTransfer(task, 1);
                        break;
//                        auto& pendingList = m_taskStatusTable->GetPendingList ();
//                        pendingList.push_back(task);
                    }
                    else if(m_slice < MilliSeconds(0)){
                        StataTransfer(task, 2);
                        break;
                    }
                }
                ReleaseResource(task, 0);
                m_slice = MilliSeconds(50);
            }
        }

    }
    void RRMachine::RunReceivedTask()
    {
//        NS_LOG_FUNCTION(this);
//		//m_taskStatusTable->CheckStataCorrect(m_task);
//		Ptr<ResourceContainer> container = m_node->GetObject<ResourceContainer>();
//		for(auto it = container->Begin();it!=container->End();++it)
//		{
//		    Ptr<Resource> resource = *it;
//            if(resource->GetMemory()>=m_task->GetRMemory()&&resource->GetCpu()>=m_task->GetRCpu())
//		    {
//		        ReceivedStataTransfer(m_task,0);
//                resource->Allocate(m_task->GetRCpu(),m_task->GetRMemory());
//                NS_LOG_INFO ("EasiEI:  At time " << Simulator::Now ().As (Time::S) << " Machine: " <<GetMachineId()<<'\t'<< " Running task  "<<m_task->GetTId() << std::endl);
//		        m_taskAllocateTable.insert(std::make_pair(m_task->GetTId(),resource->GetResourceId()));
//		        return;
//            }
//        }
//
        ;
    }
    void RRMachine::ReleaseResource(Ptr<Task> task,int i) //i==0时，释放本地Task资源，i==1时，释放网络中接收到的Task资源。
    {
//		Ptr<ResourceContainer> container = m_node->GetObject<ResourceContainer>();
//        auto resource = *(container->Begin());
//		resource->Release(m_task->GetRCpu(),m_task->GetRMemory());
//		StataTransfer(m_task,1);
        if(i==0)
        {
            Release(task);
        }
        if(i==1)
        {
            ReleaseReceived(task);
        }

    }
    /*----------------------privete------------------------------*/
    void RRMachine::Release(Ptr<Task> task)
    {
        NS_LOG_FUNCTION(this);
        auto tableit = m_taskAllocateTable.find(task->GetTId());//tableit代表m_taskAllocateTable的迭代器
                                                                // m_taskAllocateTable.erase(task->GetTId());
        if(tableit==m_taskAllocateTable.end())
        {
            std::cerr<<"Can't release an non-existent task "<<std::endl;
            abort();
        }
        std::string resourceId = tableit->second;
        ResourceContainer container = *(m_node->GetObject<ResourceContainer>());
        auto resourceIt = find_if(container.Begin(),container.End(),[resourceId](Ptr<Resource> r){return r->GetResourceId()==resourceId; });
        //auto resourceIt = find_if(container->Begin(),container->End(),[resourceId](ResourceContainer::Iterator r){return (*r)->GetResourceId()==resourceId;});
        if(resourceIt==container.End())
        {
            std::cerr<<"Can't release resource, because Resource doesn't exist"<<std::endl;
            abort();
        }
        Ptr<Resource> resource = *resourceIt;
        NS_LOG_INFO ("EasiEI:  At time " << Simulator::Now ().As (Time::S) << " Machine: " <<GetMachineId()<<'\t'<< " Release the resources occupied by the task  "<<task->GetTId() << std::endl);
        resource->Release(task->GetRCpu(),task->GetRMemory());
//        Time needTime = task->GetEndTime () - task->GetGenerateTime ();
//        if (needTime.GetSeconds () > 0)
//        {
//            StataTransfer (task, 2);
//        }
//        else
//        {
//            StataTransfer (task, 1);
//        }
    }
    void RRMachine::ReleaseReceived(Ptr<Task> task)
    {
//        NS_LOG_FUNCTION(this);
//		auto tableit = m_taskAllocateTable.find(task->GetTId());//tableit代表m_taskAllocateTable的迭代器
//       // m_taskAllocateTable.erase(task->GetTId());
//		if(tableit==m_taskAllocateTable.end())
//		{
//		    std::cerr<<"Can't release an non-existent task "<<std::endl;
//		    abort();
//		}
//		std::string resourceId = tableit->second;
//		ResourceContainer container = *(m_node->GetObject<ResourceContainer>());
//		auto resourceIt = find_if(container.Begin(),container.End(),[resourceId](Ptr<Resource> r){return r->GetResourceId()==resourceId; });
//		//auto resourceIt = find_if(container->Begin(),container->End(),[resourceId](ResourceContainer::Iterator r){return (*r)->GetResourceId()==resourceId;});
//		if(resourceIt==container.End())
//		{
//		    std::cerr<<"Can't release resource, because Resource doesn't exist"<<std::endl;
//		    abort();
//		}
//		Ptr<Resource> resource = *resourceIt;
//        NS_LOG_INFO ("EasiEI:  At time " << Simulator::Now ().As (Time::S) << " Machine: " <<GetMachineId()<<'\t'<< " Release the resources occupied by the task  "<<task->GetTId() << std::endl);
//		resource->Release(task->GetRCpu(),task->GetRMemory());
//		ReceivedStataTransfer(task,1);
    }
    void RRMachine::ReleaseInfoInsert()
    {
        //加上判断：if(task->GetOriginMId() == GetMachineId()) 表示执行的任务的生成地就是本机地址
        //因为目的地肯定是本机地址了，所以这样一判断就是生成地=目的地=本机地址，此任务就不涉及网络
        //转发
        auto it = find_if(m_taskRelease.begin(),m_taskRelease.end(),[this](Ptr<Task> t){return  m_task->GetEndTime()<=t->GetEndTime();});
        m_taskRelease.insert(it,m_task);
    }
    void RRMachine::ReleaseInfoDelete()
    {
        auto it = find_if(m_taskRelease.begin(),m_taskRelease.end(),[this](Ptr<Task> task){return  m_task->GetTId()==task->GetTId();});
        m_taskRelease.erase(it);
	    ReleaseResource(m_task,0);

    }



}
