#ifndef STATEBASE_H
#define STATEBASE_H
#include"ns3/object.h"
#include"ns3/core-module.h"
#include"ns3/ptr.h"
namespace ns3{
    class StateBase : public Object{
        public:
            virtual void StateTransfer(Ptr<StateBase> ) = 0;
            virtual void Print(){
            }
    };
}
#endif /* STATEBASE_H */
