#include"sensorbase.h"
namespace ns3{
    NS_LOG_COMPONENT_DEFINE("SensorBase");
    NS_OBJECT_ENSURE_REGISTERED(SensorBase);

    
    void SensorBase::SetNode(Ptr<Node> node)
    {
        m_node = node;
    }
    Ptr<Node> SensorBase::GetNode(void) const
    {
        return m_node;
    }

} 
