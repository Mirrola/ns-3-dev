#ifndef SENSORBASE_H
#define SENSORBASE_H
#include"ns3/object.h"
#include"ns3/ptr.h"
#include"ns3/type-id.h"
#include"ns3/node.h"
#include"ns3/traced-value.h"
namespace ns3{
    class SensorBase:public Object{ 
        public:
            //static TypeId GetTypeId(void);
            SensorBase()=default;
            void SetNode(Ptr<Node> node);
            Ptr<Node> GetNode(void) const;
            virtual void InitialMachine(std::string Id) = 0;//负责对machine_id和功能部件的绑定
            
            
        private:
            Ptr<Node> m_node;
            std::string m_machineId="";
    };
}

#endif /* SENSORBASE_H */
