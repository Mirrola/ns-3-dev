/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
#ifndef MYAPPLICATION_H
#define MYAPPLICATION_H


#include "ns3/application.h"
#include "ns3/event-id.h"
#include "ns3/ptr.h"
#include "ns3/ipv4-address.h"
#include "ns3/traced-callback.h"
#include "ns3/task.h"
#include "ns3/manager.h"
#include<map>
#include"ns3/link-service-c.h"
#include"ns3/client-factory.h"
namespace ns3 {

class Socket;
class Packet;
class LinkServiceC;

    class Client : public Application
    {
        public:
	        static TypeId GetTypeId(void);
	        
	        Client();
	        virtual ~Client();
	        
	        void SetRemote(Address ip, uint16_t port);
	
	        void SetRemote(Address addr);

            void ReceiveTask(const Task& t,Time dt);//从Manager处接受任务用于后续发送
            void ScheduleTransmit(Time dt);
            void SetMap(std::map<std::string,Address> map){
                m_machineId_IP = map;
            }


	
        
        protected:
            virtual void DoDispose(void);

        private:
            
	  	    virtual void StartApplication (void);
	  		virtual void StopApplication (void);
	  		
	  		  /**
	  		   * \brief Schedule the next packet transmission
	  		   * \param dt time interval between packets.
	  		   */
	  		void Send (void);
	  		
	  		  /**
	  		   * \brief Handle a packet reception.
	  		   *
	  		   * This function is called by lower layers.
	  		   *
	  		   * \param socket the socket the packet was received to.
	  		   */
	  		  void HandleRead (Ptr<Socket> socket);
	  		
	  		  Ptr<Socket> m_socket; //!< Socket
	  		  Address m_peerAddress; //!< Remote peer address
	  		  uint16_t m_peerPort; //!< Remote peer port
	  		  EventId m_sendEvent; //!< Event to send the next packet
	  		
	  		  /// Callbacks for tracing the packet Tx events

              std::list<std::pair<Task,Time>> m_readyToSend; 

              Time m_time;//因为Schedule是在现在的基础上再往后延长响应时间。所以要记录上一次的时间，来减一下。
              std::map<std::string,Address> m_machineId_IP;
            
              //Task m_readyToSend;
              Ptr<ClientFactory> m_factory;
              Ptr<LinkServiceC> m_service;

    };
    

}


#endif /* MYAPPLICATION_H */

