#ifndef _CLIENT_VEC_H
#define _CLIENT_VEC_H
#include"ns3/client-base.h"
#include<vector>
namespace ns3{

    class ClientVec {
        public:
            
            ClientVec(){}
            /**
             * \Responsible for m_handle initialization
             */
            void AddHandle(Ptr<ClientBase>);

            //负责顺序调用以及参数传递。调用前把参数传进去，调用完把参数拉下来。
            void Run();

            void SetInform(Ptr<Node> node);

            void SetInform(Ptr<Packet> packet);

            void SetInform(std::string content);

            void SetInform(Task task);

            void SetInform(Information inform);

            Ptr<Packet> GetPacket(void);

            size_t Size(void);

        private:
//            /**
//             * \get information from single HandleBase from m_handle
//             */
//            void PushInform(Information globalInform);
//
//            /**
//             * \get Information from the global Information object
//             */
//            void PullInform(Information &localInform);

            /**
             * \update information
             */

            std::vector<Ptr<ClientBase>> m_handle;
            
            /**
             * \This is global information maintained by HandleVec
             */
            Information m_inform;

    };



}





#endif
