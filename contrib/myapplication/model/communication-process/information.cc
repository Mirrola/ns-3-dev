#include"information.h"
namespace ns3{
    
    NS_LOG_COMPONENT_DEFINE("Information");
    NS_OBJECT_ENSURE_REGISTERED(Information);
    
    TypeId  Information::GetTypeId(void){
        static TypeId tid = TypeId("ns3::information")
            .SetParent<Object>()
            .SetGroupName("Applications")
            .AddConstructor<Information>();
            
        return tid;
    }
    
    Ptr<Node> Information::GetNode()
    {
        return m_node;
    }
    Ptr<Packet> Information::GetPacket()
    {
        return m_packet;
    }
    std::string Information::GetContent()
    {
        return m_content;
    }
    Task Information::GetTask()
    {
        return m_task;
    }

    void Information::SetNode(Ptr<Node> node)
    {
        m_node = node;
    }
    void Information::SetPacket(Ptr<Packet> packet)
    {
        m_packet = packet;
    }
    void Information::SetContent(const std::string& content)
    {
        m_content = content;
    }
    void Information::SetTask(const Task& task)
    {
        m_task = task;
    }

}
    
