#ifndef _LINK_CLIENT_H
#define _LINK_CLIENT_H
#include"ns3/socket.h"
#include"ns3/log.h"
#include"ns3/simulator.h"
#include"ns3/client-vec.h"
#include"ns3/task.h"

namespace ns3{
    
    class LinkServiceC : public Object{
        public:
            static TypeId GetTypeId(void);
            
            LinkServiceC() = default;

            //LinkService(Information inform):m_handleVec(HandleVec(inform)){}
            /**
             * \Add HandleBase to m_handleVec
             */
            void AddHandle(Ptr<ClientBase>);

            /**
             * \Initialize the task Information of the first Object in m_handleVec.
             */
            void SetInform(Task task);

            /**
             * \Initialize packet of the first Object in m_handleVec. The output of the former is the input of the latter.
             * \So we only need to initial the packet parameter for the first Ptr<HandleBase> in m_handleVec.
             */
            void SetInform(Information inform);
        
        /**
         * \For client brief create sockets 
         */
        //void CreateSocket();

        /*
         * \For application brief shut service down  
         */
        //void CloseSocket();

        /**
         * \For server abstract packet from socket and continue the next stage of processing
         */
            void SendHandle(Ptr<Socket> socket,Task t);

            /**
             * \
             */
            ClientVec m_clientVec;

            
            // 抽象一个handle链的类，该类负责加操作，以及操作间的参数传递。
        /**
         * \Client create packet and sent to its destination 
         */
        //void Send();


    };
}
#endif
