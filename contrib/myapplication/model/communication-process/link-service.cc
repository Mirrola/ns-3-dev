#include"link-service.h"
namespace ns3{
    
    NS_LOG_COMPONENT_DEFINE("LinkService");
    NS_OBJECT_ENSURE_REGISTERED(LinkService);
    
    TypeId  LinkService::GetTypeId(void){
        static TypeId tid = TypeId("ns3::LinkService")
            .SetParent<Object>()
            .SetGroupName("Applications");
            
        return tid;
    }
    
    void LinkService::AddHandle(Ptr<ServerBase> handle){
        m_serverVec.AddHandle(handle);
    }

    void LinkService::SetInform(Ptr<Node> node){
        m_serverVec.SetInform(node);
    }
    
    void LinkService::SetInform(Ptr<Packet> packet){
        m_serverVec.SetInform(packet);
    }


    void LinkService::SetInform(Information inform){
        m_serverVec.SetInform(inform);
    }



    void LinkService::ReadHandle(Ptr<Socket> socket,Ptr<Node> node){
    //void LinkService::ReadHandle(Ptr<Socket> socket,Ptr<Node> node){
        std::cout<<"get packet"<<std::endl;
        NS_LOG_FUNCTION (this << socket);
        // HandleBase在获得packet后进行创建，或者是在获得packet之后对成员进行赋值
        // 加个工厂。或者是H作为一个类链，通过一条链进行有序调用
        Ptr<Packet> packet;
        if(m_serverVec.Size() == 0){
            return;
        }
        m_serverVec.SetInform(node);
        Address from;
        Address localAddress;
        while ((packet = socket->RecvFrom (from)))
        {
            //需要初始化packet
            m_serverVec.SetInform(packet);
            socket->GetSockName (localAddress);
            if (InetSocketAddress::IsMatchingType (from))
            {
                NS_LOG_INFO ("At time " << Simulator::Now ().As (Time::S) << " server received " << packet->GetSize () << " bytes from " <<
                        InetSocketAddress::ConvertFrom (from).GetIpv4 () << " port " <<
                        InetSocketAddress::ConvertFrom (from).GetPort ());
            }
            else if (Inet6SocketAddress::IsMatchingType (from))
            {
                NS_LOG_INFO ("At time " << Simulator::Now ().As (Time::S) << " server received " << packet->GetSize () << " bytes from " <<
                        Inet6SocketAddress::ConvertFrom (from).GetIpv6 () << " port " <<
                        Inet6SocketAddress::ConvertFrom (from).GetPort ());
            }

            m_serverVec.Run();

        }
    }


    
    

}
