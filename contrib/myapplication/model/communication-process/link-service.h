#ifndef _LINK_SERVICE_H
#define _LINK_SERVICE_H
#include"ns3/socket.h"
#include"ns3/log.h"
#include"ns3/simulator.h"
#include"ns3/server-vec.h"

namespace ns3{
    
    class LinkService : public Object{
        public:
            static TypeId GetTypeId(void);
            
            LinkService() = default;

            //LinkService(Information inform):m_handleVec(HandleVec(inform)){}
            /**
             * \Add HandleBase to m_handleVec
             */
            void AddHandle(Ptr<ServerBase>);

            /**
             * \Initialize node of each object in m_handleVec
             */
            void SetInform(Ptr<Node>);

            /**
             * \Initialize packet of the first Object in m_handleVec. The output of the former is the input of the latter.
             * \So we only need to initial the packet parameter for the first Ptr<HandleBase> in m_handleVec.
             */
            void SetInform(Ptr<Packet>);
            /**
             * \Initialize the total Information of the first Object in m_handleVec.
             */
            void SetInform(Information inform);
        
        /**
         * \For client brief create sockets 
         */
        //void CreateSocket();

        /*
         * \For application brief shut service down  
         */
        //void CloseSocket();

        /**
         * \For server abstract packet from socket and continue the next stage of processing
         */
            void ReadHandle(Ptr<Socket> socket,Ptr<Node> node);

            /**
             * \
             */
            ServerVec m_serverVec;

            
            // 抽象一个handle链的类，该类负责加操作，以及操作间的参数传递。
        /**
         * \Client create packet and sent to its destination 
         */
        //void Send();


    };
}
#endif
