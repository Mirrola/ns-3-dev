#include"server-vec.h"
namespace ns3{
    
    void ServerVec::AddHandle(Ptr<ServerBase> handle)
    {
        m_handle.push_back(handle);
    }

    void ServerVec::Run()
    {
        for(size_t i = 0; i != m_handle.size(); ++i)
        {
            m_handle[i]->InitialInform(m_inform);
            m_handle[i]->Handle();
            SetInform(m_handle[i]->GetInform());  
        }
    }
//    void ServerVec::SetInform(Information &globalInform)
//    {
//        globalInform.SetNode(m_inform.GetNode());
//        globalInform.SetPacket(m_inform.GetPacket());
//        globalInform.SetContent(m_inform.GetContent());
//    }

    void ServerVec::SetInform(Ptr<Node> node)
    {
        m_inform.SetNode(node);
    }

    void ServerVec::SetInform(Ptr<Packet> packet)
    {
        m_inform.SetPacket(packet);
    }

    void ServerVec::SetInform(std::string content)
    {
        m_inform.SetContent(content);
    }
    size_t ServerVec::Size(void)
    {
        return m_handle.size();
    }

    void ServerVec::SetInform(Information inform)
    {
        m_inform.SetNode(inform.GetNode());
        m_inform.SetPacket(inform.GetPacket());
        m_inform.SetContent(inform.GetContent());
    }
}
