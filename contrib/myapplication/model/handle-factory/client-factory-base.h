#ifndef _CLIENT_FACTORY_BASE_H
#define _CLIENT_FACTORY_BASE_H
#include"ns3/client-base.h"
namespace ns3{
    struct ClientFactoryBase: public Object{

        static TypeId GetTypeId(void); 
        virtual Ptr<ClientBase> Create() = 0;
    };
}
#endif
