#include"ns3/client-factory.h"
namespace ns3{
    NS_LOG_COMPONENT_DEFINE("ClientFactory");
    NS_OBJECT_ENSURE_REGISTERED(ClientFactory);
    TypeId ClientFactory::GetTypeId(void)
    {
        static TypeId tid = TypeId("ns3::ClientFactory")
            .SetParent<Object>()
            .SetGroupName("Applications")
            .AddConstructor<ClientFactory>();
        return tid;
    }
    ClientFactory::ClientFactory(){
        m_clientFactories["Packet"] = CreateObject<PacketCreationFactory>();
        m_clientFactories["Serialize"] = CreateObject<SerializeFactory>();
    }

    Ptr<ClientBase> ClientFactory::Create(const std::string& name){
        return m_clientFactories[name]->Create();
    }

}
