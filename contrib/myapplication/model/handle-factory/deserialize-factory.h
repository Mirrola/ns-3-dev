#ifndef _DESERIALIZE_FACTORY_H
#define _DESERIALIZE_FACTORY_H
#include"ns3/deserialize-handle.h"
#include"ns3/server-factory-base.h"
namespace ns3{
    class DeserializeHandle;
    struct DeserializeFactory:virtual public ServerFactoryBase{
        static TypeId GetTypeId(void); 

        Ptr<ServerBase> Create() override{
            return CreateObject<DeserializeHandle>();
        }
    };
}
#endif
