#include"ns3/read-factory.h"
namespace ns3{

    NS_LOG_COMPONENT_DEFINE("ReadFactory");
    NS_OBJECT_ENSURE_REGISTERED(ReadFactory);

    TypeId ReadFactory::GetTypeId(void)
    {
        static TypeId tid = TypeId("ns3::ReadFactory")
            .SetParent<ServerFactoryBase>()
            .SetGroupName("Applications")
            .AddConstructor<ReadFactory>();
        return tid;
    }
    
}
