#include"ns3/serialize-factory.h"
namespace ns3{

    NS_LOG_COMPONENT_DEFINE("SerializeFactory");
    NS_OBJECT_ENSURE_REGISTERED(SerializeFactory);

    TypeId SerializeFactory::GetTypeId(void)
    {
        static TypeId tid = TypeId("ns3::SerializeFactory")
            .SetParent<ClientFactoryBase>()
            .SetGroupName("Applications")
            .AddConstructor<SerializeFactory>();
        return tid;
    }
    
}
