#ifndef _SERVER_FACTORY_BASE_H
#define _SERVER_FACTORY_BASE_H
#include"ns3/server-base.h"
namespace ns3{
    struct ServerFactoryBase: public Object{

        static TypeId GetTypeId(void); 
        virtual Ptr<ServerBase> Create() = 0;
    };
}
#endif
