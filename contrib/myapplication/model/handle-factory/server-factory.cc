#include"ns3/server-factory.h"
namespace ns3{
    NS_LOG_COMPONENT_DEFINE("ServerFactory");
    NS_OBJECT_ENSURE_REGISTERED(ServerFactory);
    TypeId ServerFactory::GetTypeId(void)
    {
        static TypeId tid = TypeId("ns3::ServerFactory")
            .SetParent<Object>()
            .SetGroupName("Applications")
            .AddConstructor<ServerFactory>();
        return tid;
    }
    ServerFactory::ServerFactory(){
        m_serverFactories["Read"] = CreateObject<ReadFactory>();
        m_serverFactories["Deserialize"] = CreateObject<DeserializeFactory>();
    }

    Ptr<ServerBase> ServerFactory::Create(const std::string& name){
        return m_serverFactories[name]->Create();
    }

}
