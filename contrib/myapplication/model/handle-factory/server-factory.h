#ifndef _MMSERVER_FACTORY_H
#define _MMSERVER_FACTORY_H
#include"ns3/read-factory.h"
#include"ns3/deserialize-factory.h"
#include<map>
#include<string>
namespace ns3{
    class ServerFactory:public Object{

        public:
            static TypeId GetTypeId(void); 
            ServerFactory();
            
            Ptr<ServerBase> Create(const std::string& name);

        private:
            std::map<std::string,Ptr<ServerFactoryBase>> m_serverFactories;
    };
}
#endif
