#include"ns3/client-base.h"
namespace ns3{
    
    NS_LOG_COMPONENT_DEFINE("ClientBase");
    NS_OBJECT_ENSURE_REGISTERED(ClientBase);
    TypeId ClientBase::GetTypeId(void)
    {
        static TypeId tid = TypeId("ns3::ClientBase")
            .SetParent<HandleBase>()
            .SetGroupName("Applications");
        return tid;
            
    }

    void ClientBase::InitialInform(Information inform){
        m_inform.SetNode(inform.GetNode());
        m_inform.SetPacket(inform.GetPacket());
        m_inform.SetContent(inform.GetContent());
        m_inform.SetTask(inform.GetTask());
    }
            
    Information ClientBase::GetInform(void)
    {
        return m_inform;
    }


}
