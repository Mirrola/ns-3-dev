#ifndef _DESERIALIZE_HANDLE_H
#define _DESERIALIZE_HANDLE_H
#include"server-base.h"
#include"ns3/task.h"
#include"ns3/manager.h"
namespace ns3{
    class DeserializeHandle : public ServerBase{

        public:
            static TypeId GetTypeId(void);

            DeserializeHandle() = default;

            //DeserializeHandle() = default;

            //DeserializeHandle(Ptr<Packet> packet, Ptr<Node> node):m_packet(packet),m_node(node){}


            virtual void Handle() override;

        private:

            std::pair<Ptr<Task>,bool> Deserialization(const std::string& t);


    };
}
#endif
