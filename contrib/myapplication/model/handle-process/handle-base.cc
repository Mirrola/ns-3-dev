#include"ns3/handle-base.h"
namespace ns3{
    
    NS_LOG_COMPONENT_DEFINE("HandleBase");
    NS_OBJECT_ENSURE_REGISTERED(HandleBase);
    TypeId HandleBase::GetTypeId(void)
    {
        static TypeId tid = TypeId("ns3::HandleBase")
            .SetParent<Object>()
            .SetGroupName("Applications");
        return tid;
            
    }

}
