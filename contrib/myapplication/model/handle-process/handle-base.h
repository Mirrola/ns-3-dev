#ifndef _HANDLE_BASE_H
#define _HANDLE_BASE_H
#include"ns3/object.h"
namespace ns3{

    class HandleBase: public Object{

        public:
            
            static TypeId GetTypeId(void);
            /**
             * \After server extract packet from socket, HandleBase is used for the next stage of processing 
             * 
             */
            virtual void Handle() = 0;
    };
    
}
#endif
