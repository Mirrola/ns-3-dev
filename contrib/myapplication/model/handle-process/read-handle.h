#ifndef _READ_HANDLE_H
#define _READ_HANDLE_H
#include"server-base.h"
namespace ns3{
    class ReadHandle :public ServerBase{

        public:
            static TypeId GetTypeId(void);

            virtual void Handle() override;

    };
}
#endif
