#ifndef _SERIALIZE_HANDLE_H
#define _SERIALIZE_HANDLE_H
#include"client-base.h"
#include"ns3/task.h"
namespace ns3{
    class SerializeHandle : public ClientBase{

        public:
            static TypeId GetTypeId(void);

            SerializeHandle() = default;

            //DeserializeHandle() = default;

            //DeserializeHandle(Ptr<Packet> packet, Ptr<Node> node):m_packet(packet),m_node(node){}


            virtual void Handle() override;

        private:

            std::string Serialization(const Task & t);


    };
}
#endif
