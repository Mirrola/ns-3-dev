#ifndef _SERVER_BASE_H
#define _SERVER_BASE_H
#include"ns3/information.h"
#include"ns3/handle-base.h"
namespace ns3{

    class ServerBase: public HandleBase{

        public:
            
            static TypeId GetTypeId(void);
            /**
             * \After server extract packet from socket, HandleBase is used for the next stage of processing 
             * 
             */
            virtual ~ServerBase(){};

            void InitialInform(Information inform);

            Information GetInform(void);
        protected: 

            Information m_inform;

    };
    
}
#endif
