/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
#ifndef LINK_MANAGER_HELPER_H
#define LINK_MANAGER_HELPER_H

#include "ns3/link-manager.h"
#include"ns3/attribute.h"
#include"ns3/object-factory.h"
#include"ns3/node-container.h"
#include"ns3/ptr.h"
#include"ns3/net-device.h"
#include"ns3/net-device-container.h"

namespace ns3 {

    class LinkManagerHelper{

        public:
            ~LinkManagerHelper();
            void Install(Ptr<Node> node) const;
            void Install(NodeContainer c) const;

        private:
            Ptr<LinkManager> DoInstall(Ptr<Node> node) const;
    };

}

#endif /* LINK_MANAGER_HELPER_H */


