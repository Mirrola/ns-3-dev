#ifndef GENERATETASKBASE_H
#define GENERATETASKBASE_H
#include"ns3/object.h"
#include"ns3/ptr.h"
#include"ns3/type-id.h"
#include"ns3/node.h"
#include"ns3/traced-value.h"
#include"ns3/task.h"
#include"ns3/task-table.h"
#include <initializer_list>

namespace ns3{
    class GenerateTaskBase:public Object{
        public:
            static TypeId GetTypeId(void);
            GenerateTaskBase(Ptr<TaskTable> tasktable,Ptr<TaskTable> sendTable,std::string Mid,Ptr<Node> node):m_taskStatusTable(tasktable),m_sendTable(sendTable),m_machineId(Mid),m_node(node){}
            virtual ~GenerateTaskBase(){}
            virtual Ptr<Task> Generate(void)=0;
            //virtual void GetInfo(std::string taskId,double requestecpu,double requestmem,std::size_t prior,const std::string& destinationMachineId);
            virtual void GetInfo(std::initializer_list<std::string> taskInfo) =0;
            void SetMachineId(std::string Id);
            std::string GetMachineId(void) const;
            
        protected:
            Ptr<TaskTable> m_taskStatusTable;
            Ptr<TaskTable> m_sendTable;
            std::string m_machineId;
            Ptr<Node> m_node;
//            std::string m_taskId;
//            double m_requestcpu;
//            double m_requestmem;
//            std::size_t m_prior;
//            std::string m_destinationMachineId = "";
    };
}
#endif /* GENERATETASKBASE_H */
