#ifndef LINK_MANAGER_H
#define LINK_MANAGER_H
#include"ns3/object.h"
#include"ns3/ptr.h"
#include"ns3/type-id.h"
#include"ns3/node.h"
#include"ns3/traced-value.h"
#include"ns3/task-table.h"
#include"ns3/link-send.h"
#include"ns3/link-task.h"
#include<initializer_list>
namespace ns3{
    class OrchestratorBase;
    class GenerateTaskBase;
    class DefaultGenerate;
    class DefaultSend;
    class LinkManager:public Object{ //Manager作为任务生成部件、接收、发送、调度部件的集合体。
        public:
            static TypeId GetTypeId(void);
            LinkManager()=default;
            ~LinkManager(){};
            /*因为任务需求比较单一所以generate部件就不需要了,从外部接受然后直接push进去
             * */
            void InitialScene(LinkTask sample,double totalTime,double send);
            void Start(void); 
            void ReceiveTask(LinkTask t);
            /*该场景对调度部件需求很低，仅需模拟执行时延，所以不需要调度部件
             * */
            void RunTask(LinkTask task);
            void SendTask(LinkTask task);
            void SetNode(Ptr<Node> node);
            /*需要按照给定的概率分布，对资源部件剩余资源量进行修改
             * */
            Ptr<Node> GetNode(void) const;
            void SetMachineId(const std::string& Id);
            void SetMap(std::map<std::string,Address> DNS);
            void StatusPrint()
            {
                std::cout<<"Sending task to next Machine!"<<std::endl;
            }
            /*-----*/
            static void Statistic(bool success)
            {
                if(success)
                {
                    ++m_succeedFail.first;
                }
                else
                {
                    ++m_succeedFail.second;
                }

            }
            void PrintResult();
            void SaveResult(std::string filePath);
            void SetResourceDis(std::map<double,double> dis);
            /*-----*/
        private:
            Ptr<TaskTable> m_taskStatusTable = CreateObject<TaskTable>();
            Ptr<TaskTable> m_receiveTable = CreateObject<TaskTable>();
            Ptr<TaskTable> m_sendTable = CreateObject<TaskTable>();
            Ptr<Node> m_node;
            std::string m_machineId="";
            Ptr<LinkSend> m_sendUnit;
            Time m_dt;
            /*每经过以下时间发送一次任务
             * */
            double m_totalTime;
            /*1.表示client要循环发送的任务 2.表示仿真的总时间 3.在给定仿真时间内，需要发送的次数
             * */
            std::tuple<LinkTask,double,double> m_clientInit;
            LinkTask m_receivedTask;
            /*剩余资源的概率分布,first表示概率值，second表示该概率值下的资源量,概率从大到小
             * */
            std::map<double,double> m_resourceDistribution;
            static std::pair<long,long> m_succeedFail ;//first表示任务成功的次数，second表示任务失败的次数
            
    };
}

#endif /* LINK_MANAGER_H */
