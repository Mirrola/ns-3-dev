#include"rr-manager.h"
namespace ns3{
    NS_LOG_COMPONENT_DEFINE("TestManager");
    NS_OBJECT_ENSURE_REGISTERED(TestManager);


    TypeId TestManager::GetTypeId(void)
    {
        static TypeId tid = TypeId("ns3::TestManager")
            .SetParent<Object>()
            .SetGroupName("MyManager")
            .AddConstructor<TestManager>();

        return tid;
    }
    void TestManager::GetTaskInfo(std::initializer_list<std::string> taskInfo)
    {
        m_generateUnit->GetInfo(taskInfo);
//        auto it = taskInfo.begin();
//        while(it!=taskInfo.end()-1)
//        {
//            ++it;
//        }
        /*----------------------bug-----------------------------*/
        /*这里逻辑不对，他一generate，那个task就直接存到tasktable里面去了，这不对
          应该把本地执行的和发往其他地方的区分开来，二者放在两个TaskTable里面，然后
          TestManager::Run，负责进行Schedule。*/
        Ptr<Task> task = m_generateUnit->Generate();
	    //m_taskStatusTable->Print();

//      std::cout<<1<<'\t';
//        Time dt = task->GetGenerateTime();
//
//        auto id = task->GetDestinationMId();
//        if(id != m_machineId)
//        {
//            SendTask(task,dt);
//        }
//        else
//        {
//            Ptr<Task> temp = m_taskStatusTable->FirstPriorInPending();
////            while(dt>=temp->GetWaitTime())
////            {
////                m_taskStatusTable->DeletePendingFirst();
////                static Ptr<Task> temp = m_taskStatusTable->FirstPriorInPending();
////            }
//            if(dt>=temp->GetGenerateTime())
//            {
//	            Time endTime = task->GetEndTime();
//
//	            auto it = find_if(m_taskRelease.begin(),m_taskRelease.end(),[task](Ptr<Task> t){return  task->GetEndTime()<=t->GetEndTime();});
//	            m_taskRelease.insert(it,task);
//	            Simulator::Schedule(dt,&TestManager::RunTask,this);
//	            if(endTime<=Seconds(500))
//	            {
//	                Simulator::Schedule(endTime,&TestManager::Release,this);
//
//	            }
//            }
//        }
    }
    void TestManager::GetTaskInfo(void)
    {
            m_generateUnit->ContinuousDiscreteDistributions();//[ min, max)上的连续均匀分布生成Task
            for(auto task:m_generateUnit->m_task)
            {
		        Time dt = task->GetGenerateTime();

		        auto id = task->GetDestinationMId();
		        if(id != m_machineId)
		        {
		            SendTask(task,dt);
		        }
		        else
		        {
		            Ptr<Task> temp = m_taskStatusTable->FirstPriorInPending();
		//            while(dt>=temp->GetWaitTime())
		//            {
		//                m_taskStatusTable->DeletePendingFirst();
		//                static Ptr<Task> temp = m_taskStatusTable->FirstPriorInPending();
		//            }
		            if(dt>=temp->GetGenerateTime())
		            {
		            Time endTime = task->GetEndTime();

		            auto it = find_if(m_taskRelease.begin(),m_taskRelease.end(),[task](Ptr<Task> t){return  task->GetEndTime()<=t->GetEndTime();});
		            m_taskRelease.insert(it,task);
		            Simulator::Schedule(dt,&TestManager::RunTask,this);
		            if(endTime<=Seconds(500))
		            {
		                Simulator::Schedule(endTime,&TestManager::Release,this);

		            }
		            }
//		            Ptr<Task> temp = m_taskStatusTable->FirstPriorInPending();
//		            while(dt>=temp->GetWaitTime())
//		            {
//		                m_taskStatusTable->DeletePendingFirst();
//		                static Ptr<Task> temp = m_taskStatusTable->FirstPriorInPending();
//		            }
//		            if(dt>=temp->GetGenerateTime())
//		            {
//		                Simulator::Schedule(dt,&TestManager::RunTask,this);
//		            }
		            //补充release函数
	            }
	    }

    }
    void TestManager::Start(void)
    {

        //这里不太好，多了一次取任务的操作
        //RunTask是直接把PendingList里面的Task循环访问全部Schedle，所以能一个一个取出来判断
        //可以在分配前就向释放表中写入数据？不好，如果资源分配失败的话会出问题
        //那就把ChooseTask的精度提高一点，每次只挑出一个任务。
//        auto task_local = m_orchestratorUnit->ChooseSingleTask(0);
//        while(task_local)
//        {
//	        Time endTime = task_local->GetEndTime();
//		    Time dt_local = task_local->GetGenerateTime();
//	        auto it = find_if(m_taskRelease.begin(),m_taskRelease.end(),[task_local](Ptr<Task> t){return  task_local->GetEndTime()<=t->GetEndTime();});
//	        m_taskRelease.insert(it,task_local);
//	        Simulator::Schedule(dt_local,&DefaultOrchestrator::Run,m_orchestratorUnit);
//            //这里要改，要把各种Run的方式区分开来。例如，一次run一个，一次run一批。
//	        Simulator::Schedule(endTime,&TestManager::Release,this);
//            Ptr<Task> task_local = m_orchestratorUnit->ChooseSingleTask(0);
//        }
        RunTask();
        SendTask();
//        auto task = m_orchestratorUnit->ChooseSingleTask(1);
//        while(task)
//        {
//	        Time endTime = task->GetEndTime();
//		    Time dt = task->GetGenerateTime();
//
//            //这里要改，需要根据不同的machine_id在application里面创建不同的port，并绑定相应的目的
//            //ip和port，进行转发。
//            SendTask(task,dt);
//            auto task = m_orchestratorUnit->ChooseSingleTask(1);
//        }

    }

    void TestManager::GenerateTask(void)
    {
        auto task = m_generateUnit->Generate();
		Time dt = task->GetGenerateTime();
        auto id = task->GetDestinationMId();
        if(id != "")
        {
            //SendTask(t);
            //
        }

    }
    bool TestManager::ReceiveTask(Ptr<Task> t)
    {
        //先判断t任务的执行时间，如果小于当前时间。。。。就丢掉？
//        auto success =  m_receiveUnit->ReceiveTask(t);
//        Ptr<Task> task = t;
////      std::cout<<1<<'\t';
//        Time dt = task->GetGenerateTime();
//        Ptr<Task> temp = m_taskStatusTable->FirstPriorInPending();
////        while(dt>=temp->GetWaitTime())
////        {
////            m_taskStatusTable->DeletePendingFirst();
////            static Ptr<Task> temp = m_taskStatusTable->FirstPriorInPending();
////        }
//        if(dt>=temp->GetGenerateTime())
//        {
//        Time endTime = task->GetEndTime();
//
//        auto it = find_if(m_taskRelease.begin(),m_taskRelease.end(),[task](Ptr<Task> t){return  task->GetEndTime()<=t->GetEndTime();});
//        m_taskRelease.insert(it,task);
//        Simulator::Schedule(dt-Now(),&TestManager::RunTask,this);
//        if(endTime<=Seconds(500))
//        {
//            Simulator::Schedule(endTime-Now(),&TestManager::Release,this);
//
//        }
//        }
        //因为目前缺乏任务的发送时间，只有任务的调度时间。所以暂时先在时间上加个1,来抵消掉packet在信道上传输的时延。
        Time dt = t->GetGenerateTime();
        Time now = Now();
            std::cout<<"--------------------------------"<<std::endl;
            std::cout<<dt<<'\t'<<now<<std::endl;
            std::cout<<"--------------------------------"<<std::endl;
        //manager在reveive之前先对任务的创建时间进行判断。假如创建时间小于当前时间，说明任务来得晚了，可能因为节点故障也可能因为链路拥堵。那么此时任务的执行策略是否应该发生变化？应该如何变化？（任务的优先级动态变化？任务再次进行转发？或者传输过程中对任务进行备份，超过一定时间就在备份节点上直接执行？）（这应该是可靠性方面的问题了吧）
        if(dt<now)
        {
            t->SetGenerateTime(0);
            m_receiveUnit->ReceiveFromOthers(t);
            Time endTime =t->GetEndTime();
            auto it = find_if(m_remoteRelease.begin(),m_remoteRelease.end(),[t](Ptr<Task> task){return  t->GetEndTime()<=task->GetEndTime();});
            m_remoteRelease.insert(it,t);
          //Simulator::Schedule(Now(),&TestManager::RunReceivedTask,this);
            Simulator::ScheduleNow(&TestManager::RunReceivedTask,this);
            Simulator::Schedule(t->GetEndTime()-Now(),&TestManager::RemoteRelease,this);
            return false;
        }
        else
        {
        //这里m_receiveUnit->ReceiveTask()到的task应该单独存放起来，与本地调度的任务区分开来。现在先receive一个就立即Schedule一个。
            m_receiveUnit->ReceiveFromOthers(t);
//            Ptr<Task> task = m_taskStatusTable->FirstPriorInPending();
//            m_taskStatusTable->DeletePendingFirst();
            Time endTime =t->GetEndTime();
            auto it = find_if(m_taskRelease.begin(),m_taskRelease.end(),[t](Ptr<Task> task){return  t->GetEndTime()<=task->GetEndTime();});
            m_taskRelease.insert(it,t);
            Simulator::Schedule(Seconds(0),&TestManager::RunReceivedTask,this);
            Simulator::Schedule(t->GetEndTime()-Now(),&TestManager::RemoteRelease,this);
            return true;
        }

    }
    void TestManager::SendTask(Ptr<Task> t,Time dt)
    {
        //默认client安装在0,server安装在1
        const Task task = *t;
        auto client = DynamicCast<Client>(m_node->GetApplication(0));
        client->ReceiveTask(task,dt);

    }
    void TestManager::RunTask(void)
    {
        m_orchestratorUnit->ChooseTask();
   //     m_orchestratorUnit->Run();
    }
    void TestManager::SendTask(void)
    {
        m_sendUnit->Start();
    }
    void TestManager::RunSingleTask(void)
    {
       // m_orchestratorUnit->Run();
    }
    void TestManager::RunReceivedTask(void)
    {
        m_orchestratorUnit->ChooseReceivedTask();
        m_orchestratorUnit->RunReceivedTask();
    }

    void TestManager::ShowTaskStatus(void)
    {
        m_taskStatusTable->Print();
    }
    void TestManager::Release()
    {
        Time t = Simulator::Now();
        auto it = find_if(m_taskRelease.begin(),m_taskRelease.end(),[t](Ptr<Task> task){return  t<=task->GetEndTime();});
	    auto task = *(it);
        m_taskRelease.erase(it);
	    m_orchestratorUnit->ReleaseResource(task,0);

    }
    void TestManager::RemoteRelease()
    {
        Time t = Simulator::Now();
        auto it = find_if(m_remoteRelease.begin(),m_remoteRelease.end(),[t](Ptr<Task> task){return  t<=task->GetEndTime();});
	    auto task = *(it);
        m_remoteRelease.erase(it);
	    m_orchestratorUnit->ReleaseResource(task,1);

    }
    void TestManager::SetNode(Ptr<Node> node)
    {
        m_node = node;
        m_orchestratorUnit = Create<TestOrchestrator>(m_taskStatusTable,m_receiveTable,m_sendTable,m_node,m_machineId);
        m_generateUnit = Create<DefaultGenerate>(m_taskStatusTable,m_sendTable,m_machineId,m_node);
        m_receiveUnit = Create<DefaultReceive>(m_taskStatusTable,m_receiveTable,m_machineId);
        m_sendUnit = Create<DefaultSend>(m_sendTable,m_machineId,m_node);
    }
    Ptr<Node> TestManager::GetNode(void) const
    {
        return m_node;
    }
    void TestManager::SetMachineId(std::string Id)
    {
        m_machineId = Id;//还没改完，部件内的Id也要改
        m_generateUnit->SetMachineId(Id);
        m_receiveUnit->SetMachineId(Id);
        m_orchestratorUnit->SetMachineId(Id);
    }
    void TestManager::SetMap(std::map<std::string,Address> DNS)
    {
        m_sendUnit->SetMap(DNS);  //DNS功能转入application，sendUnit把该功能删除
        //auto client = DynamicCast<Client>(m_node->GetApplication(0));
        //client->SetMap(DNS);
    }
    void TestManager::SetSETime(double start,double end)
    {
        m_sendUnit->SetClientTime(start,end);
    }

}
