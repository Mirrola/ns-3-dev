#include"ns3/default-orchestrator.h"
#include"ns3/log.h"
namespace ns3{
    NS_LOG_COMPONENT_DEFINE("DefaultOrchestrator");
    NS_OBJECT_ENSURE_REGISTERED(DefaultOrchestrator);
    
    /*--------------------------public--------------------------*/
    bool DefaultOrchestrator::ChooseTask() 
    {
        if(m_taskStatusTable->PendingEmpty())
        {
            std::cerr<<" has no pending task to run. "<<std::endl;
            return 0;
        }
        auto list = m_taskStatusTable->GetPendingList();
//                m_task = *list.begin();
        for(auto it = list.begin();it!=list.end();++it)
        {
            Simulator::Schedule((*it)->GetGenerateTime(),&DefaultOrchestrator::Run,this,*it);
           // Simulator::Schedule(m_task->GetGenerateTime(),&DefaultOrchestrator::ReleaseInfoInsert,this);
           //感觉没有必要再加这么一个Release的list来记录信息了，orche里面本来就有taskallocateble来记录
           //运行了的任务的信息。
            Simulator::Schedule((*it)->GetEndTime(),&DefaultOrchestrator::ReleaseResource,this,*it,0);
          //  Simulator::Schedule(m_task->GetEndTime(),&DefaultOrchestrator::ReleaseInfoDelete,this);
        }
        return 1;
    }
    bool DefaultOrchestrator::ChooseReceivedTask() 
    {              
		if(m_receiveTable->PendingEmpty())
		{
		    std::cerr<<" has no pending task to run. "<<std::endl;
		    return 0;
		}
        auto list = m_receiveTable->GetPendingList();
        m_task = list.front();
//        m_task = *list.begin();
//        for(auto it = list.begin();it!=list.end();++it)
//        {
//				    m_task = *it;
//            Simulator::Schedule(m_task->GetGenerateTime(),&DefaultOrchestrator::RunReceivedTask,this);
//        }
	    return 1;
	}
    Ptr<Task> DefaultOrchestrator::ChooseSingleTask(size_t i)//i==0表示m_taskStatusTable,i==1表示sendTable
    {
        if(i ==0)
        {

	        if(m_taskStatusTable->PendingEmpty())
	        {
			    std::cerr<<" has no pending task to run. "<<std::endl;
	            return nullptr;
	        }
	        auto list = m_taskStatusTable->GetPendingList();
	        m_task = list.front();
	        list.pop_front();
	        return m_task;
        }
        else 
        {
	        if(m_sendTable->PendingEmpty())
	        {
			    std::cerr<<" has no pending task to run. "<<std::endl;
	            return nullptr;
	        }
	        auto list = m_sendTable->GetPendingList();
	        m_task = list.front();
	        list.pop_front();
	        return m_task;
        }
    }
    void DefaultOrchestrator::StataTransfer(Ptr<Task> t,int i)
    {
		if(i==0)
		{
		    m_taskStatusTable->PendingToRunning(t);

		}
		else
		{
		    m_taskStatusTable->RunningToDead(t);
		}

    }
    void DefaultOrchestrator::ReceivedStataTransfer(Ptr<Task> t,int i)
    {
		if(i==0)
		{
		    m_receiveTable->PendingToRunning(t);
		}
		else
		{
		    m_receiveTable->RunningToDead(t);
		}

    }
    void DefaultOrchestrator::Run(Ptr<Task> task) 
    {
        NS_LOG_FUNCTION(this);
		if(!m_taskStatusTable->CheckIn(task))
        {
            return;
        }
		//m_taskStatusTable->CheckStataCorrect(m_task);
		Ptr<ResourceContainer> container = m_node->GetObject<ResourceContainer>();
		for(auto it = container->Begin();it!=container->End();++it)
		{
		    Ptr<Resource> resource = *it;
            NS_LOG_INFO ("EasiEI:  At time " << Simulator::Now ().As (Time::S) << " Machine: " <<GetMachineId()<<'\t'<< "  Running task  "<<task->GetTId() << std::endl);
		    if(resource->Allocate(task->GetRCpu(),task->GetRMemory())==true)
		    {
		        StataTransfer(task,0);
		        m_taskAllocateTable.insert(std::make_pair(task->GetTId(),resource->GetResourceId()));
		        return;
            }
        }

    }
    void DefaultOrchestrator::RunReceivedTask() 
    {
        NS_LOG_FUNCTION(this);
		//m_taskStatusTable->CheckStataCorrect(m_task);
		Ptr<ResourceContainer> container = m_node->GetObject<ResourceContainer>();
		for(auto it = container->Begin();it!=container->End();++it)
		{
		    Ptr<Resource> resource = *it;
            if(resource->GetMemory()>=m_task->GetRMemory()&&resource->GetCpu()>=m_task->GetRCpu())
		    {
		        ReceivedStataTransfer(m_task,0);
                resource->Allocate(m_task->GetRCpu(),m_task->GetRMemory());
                NS_LOG_INFO ("EasiEI:  At time " << Simulator::Now ().As (Time::S) << " Machine: " <<GetMachineId()<<'\t'<< " Running task  "<<m_task->GetTId() << std::endl);
		        m_taskAllocateTable.insert(std::make_pair(m_task->GetTId(),resource->GetResourceId()));
		        return; 
            }
        }

    }
    void DefaultOrchestrator::ReleaseResource(Ptr<Task> task,int i) //i==0时，释放本地Task资源，i==1时，释放网络中接收到的Task资源。
    {
//		Ptr<ResourceContainer> container = m_node->GetObject<ResourceContainer>();
//        auto resource = *(container->Begin());
//		resource->Release(m_task->GetRCpu(),m_task->GetRMemory());
//		StataTransfer(m_task,1);
        if(i==0)
        {
            Release(task);
        }
        if(i==1)
        {
            ReleaseReceived(task);
        }
		
    }
    /*----------------------privete------------------------------*/
    void DefaultOrchestrator::Release(Ptr<Task> task) 
    {
        NS_LOG_FUNCTION(this);
		auto tableit = m_taskAllocateTable.find(task->GetTId());//tableit代表m_taskAllocateTable的迭代器
       // m_taskAllocateTable.erase(task->GetTId());
		if(tableit==m_taskAllocateTable.end())
		{
		    std::cerr<<"Can't release an non-existent task "<<std::endl;
		    abort();
		}
		std::string resourceId = tableit->second;
		ResourceContainer container = *(m_node->GetObject<ResourceContainer>());
		auto resourceIt = find_if(container.Begin(),container.End(),[resourceId](Ptr<Resource> r){return r->GetResourceId()==resourceId; });       
		//auto resourceIt = find_if(container->Begin(),container->End(),[resourceId](ResourceContainer::Iterator r){return (*r)->GetResourceId()==resourceId;});
		if(resourceIt==container.End())
		{
		    std::cerr<<"Can't release resource, because Resource doesn't exist"<<std::endl;
		    abort();
		}
		Ptr<Resource> resource = *resourceIt;
        NS_LOG_INFO ("EasiEI:  At time " << Simulator::Now ().As (Time::S) << " Machine: " <<GetMachineId()<<'\t'<< " Release the resources occupied by the task  "<<task->GetTId() << std::endl);
		resource->Release(task->GetRCpu(),task->GetRMemory());
		StataTransfer(task,1);
    }
    void DefaultOrchestrator::ReleaseReceived(Ptr<Task> task) 
    {
        NS_LOG_FUNCTION(this);
		auto tableit = m_taskAllocateTable.find(task->GetTId());//tableit代表m_taskAllocateTable的迭代器
       // m_taskAllocateTable.erase(task->GetTId());
		if(tableit==m_taskAllocateTable.end())
		{
		    std::cerr<<"Can't release an non-existent task "<<std::endl;
		    abort();
		}
		std::string resourceId = tableit->second;
		ResourceContainer container = *(m_node->GetObject<ResourceContainer>());
		auto resourceIt = find_if(container.Begin(),container.End(),[resourceId](Ptr<Resource> r){return r->GetResourceId()==resourceId; });       
		//auto resourceIt = find_if(container->Begin(),container->End(),[resourceId](ResourceContainer::Iterator r){return (*r)->GetResourceId()==resourceId;});
		if(resourceIt==container.End())
		{
		    std::cerr<<"Can't release resource, because Resource doesn't exist"<<std::endl;
		    abort();
		}
		Ptr<Resource> resource = *resourceIt;
        NS_LOG_INFO ("EasiEI:  At time " << Simulator::Now ().As (Time::S) << " Machine: " <<GetMachineId()<<'\t'<< " Release the resources occupied by the task  "<<task->GetTId() << std::endl);
		resource->Release(task->GetRCpu(),task->GetRMemory());
		ReceivedStataTransfer(task,1);
    }
    void DefaultOrchestrator::ReleaseInfoInsert()
    {
        //加上判断：if(task->GetOriginMId() == GetMachineId()) 表示执行的任务的生成地就是本机地址
        //因为目的地肯定是本机地址了，所以这样一判断就是生成地=目的地=本机地址，此任务就不涉及网络
        //转发
        auto it = find_if(m_taskRelease.begin(),m_taskRelease.end(),[this](Ptr<Task> t){return  m_task->GetEndTime()<=t->GetEndTime();});
        m_taskRelease.insert(it,m_task);
    }
    void DefaultOrchestrator::ReleaseInfoDelete()
    {
        auto it = find_if(m_taskRelease.begin(),m_taskRelease.end(),[this](Ptr<Task> task){return  m_task->GetTId()==task->GetTId();});
        m_taskRelease.erase(it);
	    ReleaseResource(m_task,0);

    }



}
