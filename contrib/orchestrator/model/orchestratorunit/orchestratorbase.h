#ifndef ORCHESTRATORBASE_H
#define ORCHESTRATORBASE_H
#include"ns3/object.h"
#include"ns3/ptr.h"
#include"ns3/type-id.h"
#include"ns3/node.h"
#include"ns3/traced-value.h"
#include"ns3/resource-unit-container.h"
#include"ns3/resource-unit.h"
#include"ns3/task-table.h"
namespace ns3{
    class SendBase;
    class TaskTable;
    class Task;
    class OrchestratorBase:public Object{
        public:
            static TypeId GetTypeId(void);
            OrchestratorBase(Ptr<TaskTable> local,Ptr<TaskTable> remote,Ptr<TaskTable> send,Ptr<Node> node,std::string machineId):m_taskStatusTable(local),m_receiveTable(remote),m_sendTable(send),m_node(node),m_machineId(machineId){}
            virtual ~OrchestratorBase(){}
            virtual bool ChooseTask()=0;//按照某种规则挑选出任务
            virtual bool ChooseReceivedTask()=0;//从接收到的Task种挑选一个后续为其分配资源
            virtual void Run(Ptr<Task> task)=0;//运行挑选出的任务
            virtual void RunReceivedTask() = 0;
            virtual void Release(Ptr<Task> task)=0;//任务结束时，释放占用的资源
            virtual void ReleaseReceived(Ptr<Task> task)=0;//任务结束时，释放占用的资源
            void SetNode(Ptr<Node> node);
            Ptr<Node> GetNode(void) const;
            void SetMachineId(std::string Id);
            std::string GetMachineId(void) const;

        protected:
            Ptr<TaskTable> m_taskStatusTable;
            Ptr<TaskTable> m_receiveTable;
            Ptr<TaskTable> m_sendTable;
            Ptr<Node> m_node;
            std::string m_machineId;
    };
}
#endif  /*  ORCHESTRATORBASE_H  */
