#include"ns3/rr-orchestrator.h"
#include"ns3/log.h"
namespace ns3{
    NS_LOG_COMPONENT_DEFINE("TestOrchestrator");
    NS_OBJECT_ENSURE_REGISTERED(TestOrchestrator);

    /*--------------------------public--------------------------*/
    bool TestOrchestrator::ChooseTask()
    {
        if(m_taskStatusTable->PendingEmpty())
        {
            std::cerr<<" has no pending task to run. "<<std::endl;
            return 0;
        }
        Simulator::Schedule (Simulator::Now (), &TestOrchestrator::oneRoundDispatch, this);
        return 1;
    }
    void TestOrchestrator::Transit (Time dt)
    {
        NS_LOG_FUNCTION (this << dt);
        // auto pendingList = m_taskStatusTable->GetPendingList ();
        // auto runningList = m_taskStatusTable->GetRunningList ();
        Simulator::Schedule (dt, &TestOrchestrator::oneRoundDispatch, this);
    }
    void TestOrchestrator::oneRoundDispatch ()
    {
        Time slice = Seconds (1000.0);
        Time time = Simulator::Now ();
        NS_LOG_INFO (Simulator::Now ());
        auto pendingList = m_taskStatusTable->GetPendingList ();
        auto runningList = m_taskStatusTable->GetRunningList ();
        if (pendingList.empty () == true)
        {
            return;
        }
        std::list<Ptr<Task>>::iterator it = pendingList.begin ();
        // Ptr<Task> t = *it;
        // std::list<Ptr<Task>>::iterator it2 = runningList.begin ();
        // std::pair<std::list<Ptr<Task>>::iterator, std::list<Ptr<Task>>::iterator> result =
        //     getIterator (m_taskStatusTable);
        // std::list<Ptr<Task>>::iterator it1 = getIterator (m_taskStatusTable);
        Time needTime = (*it)->GetEndTime () - (*it)->GetGenerateTime ();
        Time cur_time = std::min (needTime, slice);
        if ((*it)->GetGenerateTime () < time)
        {
            double timeGN = time.GetSeconds () - (*it)->GetGenerateTime ().GetSeconds ();
            (*it)->SetGenerateTime (time.GetSeconds ());
            (*it)->SetEndTime ((*it)->GetEndTime ().GetSeconds () + timeGN);
        }
        if (needTime.GetSeconds () > 0)
        {
            // Simulator::Schedule ((*it)->GetGenerateTime (), &TestOrchestrator::Run, this, *it);
            // Simulator::Schedule ((*it)->GetGenerateTime ()+cur_time, &TestOrchestrator::ReleaseResource, this, *it, 0);
            Simulator::Schedule (Seconds (0), &TestOrchestrator::Run, this, *it);
            Simulator::Schedule (cur_time, &TestOrchestrator::ReleaseResource, this, *it, 0);
            double generateTime = ((*it)->GetGenerateTime () + cur_time).GetSeconds ();
            (*it)->SetGenerateTime (generateTime);
            // Simulator::Schedule (time, oneRoundDispatch (it, this), this);

            // std::pair<Task, std::list<Ptr<Task>>::iterator> result = dequeue (it2, runningList);
            // Task t = result.first;
            // it2 = result.second;
            // it1 = enqueue (it1, list, t);
        }
        if (pendingList.empty () != true)
        {
            Transit(cur_time);
        }
    }
    bool TestOrchestrator::ChooseReceivedTask()
    {
		if(m_receiveTable->PendingEmpty())
		{
		    std::cerr<<" has no pending task to run. "<<std::endl;
		    return 0;
		}
        auto list = m_receiveTable->GetPendingList();
        m_task = list.front();
//        m_task = *list.begin();
//        for(auto it = list.begin();it!=list.end();++it)
//        {
//				    m_task = *it;
//            Simulator::Schedule(m_task->GetGenerateTime(),&TestOrchestrator::RunReceivedTask,this);
//        }
	    return 1;
	}
    Ptr<Task> TestOrchestrator::ChooseSingleTask(size_t i)//i==0表示m_taskStatusTable,i==1表示sendTable
    {
        if(i ==0)
        {

	        if(m_taskStatusTable->PendingEmpty())
	        {
			    std::cerr<<" has no pending task to run. "<<std::endl;
	            return nullptr;
	        }
	        auto list = m_taskStatusTable->GetPendingList();
	        m_task = list.front();
	        list.pop_front();
	        return m_task;
        }
        else
        {
	        if(m_sendTable->PendingEmpty())
	        {
			    std::cerr<<" has no pending task to run. "<<std::endl;
	            return nullptr;
	        }
	        auto list = m_sendTable->GetPendingList();
	        m_task = list.front();
	        list.pop_front();
	        return m_task;
        }
    }
    void TestOrchestrator::StataTransfer (Ptr<Task> t, int i)
    {
      if (i == 0)
      {
          m_taskStatusTable->PendingToRunning (t);
      }
      else if (i == 1)
      {
          m_taskStatusTable->RunningToDead (t);
      }
      else
      {
          m_taskStatusTable->RunningToPending (t);
      }
    }
    void TestOrchestrator::ReceivedStataTransfer (Ptr<Task> t, int i)
    {
      if (i == 0)
      {
          m_receiveTable->PendingToRunning (t);
      }
      else if (i == 1)
      {
          m_receiveTable->RunningToDead (t);
      }
      else
      {
          m_receiveTable->RunningToPending (t);
      }
    }
    void TestOrchestrator::Run(Ptr<Task> task)
    {
        NS_LOG_FUNCTION(this);
		if(!m_taskStatusTable->CheckIn(task))
        {
            return;
        }
		//m_taskStatusTable->CheckStataCorrect(m_task);
		Ptr<ResourceContainer> container = m_node->GetObject<ResourceContainer>();
		for(auto it = container->Begin();it!=container->End();++it)
		{
		    Ptr<Resource> resource = *it;
            NS_LOG_INFO ("EasiEI:  At time " << Simulator::Now ().As (Time::S) << " Machine: " <<GetMachineId()<<'\t'<< "  Running task  "<<task->GetTId() << std::endl);
		    if(resource->Allocate(task->GetRCpu(),task->GetRMemory())==true)
		    {
            // auto pendingList = m_taskStatusTable->GetPendingList ();
            // auto runningList = m_taskStatusTable->GetRunningList ();
		        StataTransfer(task,0);
            // pendingList = m_taskStatusTable->GetPendingList ();
            // runningList = m_taskStatusTable->GetRunningList ();
		        m_taskAllocateTable.insert(std::make_pair(task->GetTId(),resource->GetResourceId()));
		        return;
            }
        }

    }
    void TestOrchestrator::RunReceivedTask()
    {
        NS_LOG_FUNCTION(this);
		//m_taskStatusTable->CheckStataCorrect(m_task);
		Ptr<ResourceContainer> container = m_node->GetObject<ResourceContainer>();
		for(auto it = container->Begin();it!=container->End();++it)
		{
		    Ptr<Resource> resource = *it;
            if(resource->GetMemory()>=m_task->GetRMemory()&&resource->GetCpu()>=m_task->GetRCpu())
		    {
		        ReceivedStataTransfer(m_task,0);
                resource->Allocate(m_task->GetRCpu(),m_task->GetRMemory());
                NS_LOG_INFO ("EasiEI:  At time " << Simulator::Now ().As (Time::S) << " Machine: " <<GetMachineId()<<'\t'<< " Running task  "<<m_task->GetTId() << std::endl);
		        m_taskAllocateTable.insert(std::make_pair(m_task->GetTId(),resource->GetResourceId()));
		        return;
            }
        }

    }
    void TestOrchestrator::ReleaseResource(Ptr<Task> task,int i) //i==0时，释放本地Task资源，i==1时，释放网络中接收到的Task资源。
    {
//		Ptr<ResourceContainer> container = m_node->GetObject<ResourceContainer>();
//        auto resource = *(container->Begin());
//		resource->Release(m_task->GetRCpu(),m_task->GetRMemory());
//		StataTransfer(m_task,1);
        if(i==0)
        {
            Release(task);
        }
        if(i==1)
        {
            ReleaseReceived(task);
        }

    }
    /*----------------------privete------------------------------*/
    void TestOrchestrator::Release(Ptr<Task> task)
    {
        NS_LOG_FUNCTION(this);
		auto tableit = m_taskAllocateTable.find(task->GetTId());//tableit代表m_taskAllocateTable的迭代器
       // m_taskAllocateTable.erase(task->GetTId());
		if(tableit==m_taskAllocateTable.end())
		{
		    std::cerr<<"Can't release an non-existent task "<<std::endl;
		    abort();
		}
		std::string resourceId = tableit->second;
		ResourceContainer container = *(m_node->GetObject<ResourceContainer>());
		auto resourceIt = find_if(container.Begin(),container.End(),[resourceId](Ptr<Resource> r){return r->GetResourceId()==resourceId; });
		//auto resourceIt = find_if(container->Begin(),container->End(),[resourceId](ResourceContainer::Iterator r){return (*r)->GetResourceId()==resourceId;});
		if(resourceIt==container.End())
		{
		    std::cerr<<"Can't release resource, because Resource doesn't exist"<<std::endl;
		    abort();
		}
		Ptr<Resource> resource = *resourceIt;
        NS_LOG_INFO ("EasiEI:  At time " << Simulator::Now ().As (Time::S) << " Machine: " <<GetMachineId()<<'\t'<< " Release the resources occupied by the task  "<<task->GetTId() << std::endl);
		resource->Release(task->GetRCpu(),task->GetRMemory());
    Time needTime = task->GetEndTime () - task->GetGenerateTime ();
    if (needTime.GetSeconds () > 0)
    {
        StataTransfer (task, 2);
        // auto pendingList = m_taskStatusTable->GetPendingList ();
        // auto runningList = m_taskStatusTable->GetRunningList ();
    }
    else
    {
        StataTransfer (task, 1);
        // auto pendingList = m_taskStatusTable->GetPendingList ();
        // auto runningList = m_taskStatusTable->GetRunningList ();
    }
    }
    void TestOrchestrator::ReleaseReceived(Ptr<Task> task)
    {
        NS_LOG_FUNCTION(this);
		auto tableit = m_taskAllocateTable.find(task->GetTId());//tableit代表m_taskAllocateTable的迭代器
       // m_taskAllocateTable.erase(task->GetTId());
		if(tableit==m_taskAllocateTable.end())
		{
		    std::cerr<<"Can't release an non-existent task "<<std::endl;
		    abort();
		}
		std::string resourceId = tableit->second;
		ResourceContainer container = *(m_node->GetObject<ResourceContainer>());
		auto resourceIt = find_if(container.Begin(),container.End(),[resourceId](Ptr<Resource> r){return r->GetResourceId()==resourceId; });
		//auto resourceIt = find_if(container->Begin(),container->End(),[resourceId](ResourceContainer::Iterator r){return (*r)->GetResourceId()==resourceId;});
		if(resourceIt==container.End())
		{
		    std::cerr<<"Can't release resource, because Resource doesn't exist"<<std::endl;
		    abort();
		}
		Ptr<Resource> resource = *resourceIt;
        NS_LOG_INFO ("EasiEI:  At time " << Simulator::Now ().As (Time::S) << " Machine: " <<GetMachineId()<<'\t'<< " Release the resources occupied by the task  "<<task->GetTId() << std::endl);
		resource->Release(task->GetRCpu(),task->GetRMemory());
		ReceivedStataTransfer(task,1);
    }
    void TestOrchestrator::ReleaseInfoInsert()
    {
        //加上判断：if(task->GetOriginMId() == GetMachineId()) 表示执行的任务的生成地就是本机地址
        //因为目的地肯定是本机地址了，所以这样一判断就是生成地=目的地=本机地址，此任务就不涉及网络
        //转发
        auto it = find_if(m_taskRelease.begin(),m_taskRelease.end(),[this](Ptr<Task> t){return  m_task->GetEndTime()<=t->GetEndTime();});
        m_taskRelease.insert(it,m_task);
    }
    void TestOrchestrator::ReleaseInfoDelete()
    {
        auto it = find_if(m_taskRelease.begin(),m_taskRelease.end(),[this](Ptr<Task> task){return  m_task->GetTId()==task->GetTId();});
        m_taskRelease.erase(it);
	    ReleaseResource(m_task,0);

    }



}
