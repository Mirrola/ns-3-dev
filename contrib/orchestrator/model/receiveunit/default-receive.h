#ifndef DEFAULTRECEIVE
#define DEFAULTRECEIVE
#include"ns3/receivebase.h"
#include"ns3/task.h"
#include"ns3/core-module.h"
namespace ns3{
    class DefaultReceive;
    class DefaultReceive:public ReceiveBase{
        public:
            DefaultReceive(Ptr<TaskTable> local,Ptr<TaskTable> remote,std::string mid):ReceiveBase(local,remote,mid){}

            bool ReceiveTask(Ptr<Task> task);//这一部分不在基类中声明是可能接收到的不是task而是一个job(由多个task组成)，接收的形式都大不一样
            bool ReceiveFromOthers(Ptr<Task> task);
            virtual bool HandleReceive(void) override;//这个判断还得改
        private:
            Ptr<Task> m_task;

    };
}
#endif 
