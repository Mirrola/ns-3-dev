/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
#ifndef LINK_SCENARIO_H
#define LINK_SCENARIO_H
#include "ns3/link-client.h"
#include "ns3/link-server.h"
#include "linkmanager-helper.h"
#include "link-manager.h"
#include "link-orchestrator.h"
#include "link-send.h"
#include "link-task.h"

/*EasiEi场景描述
 * 
 * 可靠性场景
//       
// m0 -------------- m1 -------------- m2 -------------- m3
//    point-to-point     point-to-point    point-to-point
//
//    以上为scratch/link-network.cc的网络拓扑，m0、m1、m2、
// m3分别代表具有计算资源（cpu）的设备，场景中存在一个模板task，
// 该task经由m0,1,2,3分别执行后，如果总时延（包含任务执行时延
// 和任务传输时延）大于任务本身规定的最长等待时间，那么任务则认定
// 为失败，重复实验n次分别统计成功与失败的次数。
//    变量：设备的计算资源、网络的带宽根据给定的概率分布取得。
 * */
#endif /* LINK_SCENARIO_H */

