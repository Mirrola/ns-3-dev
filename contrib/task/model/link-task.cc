#include "ns3/link-task.h"
namespace ns3{
    NS_LOG_COMPONENT_DEFINE("LinkTask");
    NS_OBJECT_ENSURE_REGISTERED(LinkTask);
    
    void LinkTask::InitialMachineList(std::initializer_list<std::string> machineid)
    {
        for(auto i:machineid)
        {
            m_machine_list.push_back(i);
        }
    }
    std::list<std::string> LinkTask::GetMachineList()
    {
        return m_machine_list;
    }
    std::string LinkTask::PopMachine()
    {
        auto t =m_machine_list.front();
        m_machine_list.pop_front();
        return t;
    }
    std::string LinkTask::GetMachine()
    {
        auto t = m_machine_list.front();
        return t;
    }
}
