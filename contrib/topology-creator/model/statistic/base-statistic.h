#include<iostream>
namespace ns3{
	class StatisticBase{ 
	    public:
            virtual ~StatisticBase(){}
	        void SetFilePath(std::string file);
	        std::string GetFilePath() const;
	        virtual void ReadFile()=0;
	    private:
	        //virtual void DoStatistic(std::string data)=0;
	        std::string m_fileName="";
	
	};
}
