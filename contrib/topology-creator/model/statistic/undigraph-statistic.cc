#include"ns3/undigraph-statistic.h"
#include<algorithm>
#include<string>
namespace ns3{
    void UndigraphStatistic::SetListFile(std::string file)
    {
        m_listFile=file;
    }
    void UndigraphStatistic::ReadListFile()
    {
        std::string file = m_listFile;
	    std::ifstream in(file);
	    std::string line;
	    getline(in,line); //去掉标签项
        int i=0;
	    while(getline(in,line))
	    {
	        std::stringstream ss(line);
	        std::string str;
	        getline(ss,str,',');//去掉表中的行下标
	        getline(ss,str,',');
	        m_switch[i] =str;
            ++i;
        }
    }
    void UndigraphStatistic::ReadFile()
    {
        std::string file = GetFilePath();
	    std::ifstream in(file);
	    std::string line;
	    getline(in,line); //去掉标签项
	    while(getline(in,line))
	    {
	        std::stringstream ss(line);
	        std::string str;
	        getline(ss,str,',');
	        getline(ss,str,',');
	        std::string switch_x =str;
	        getline(ss,str,',');
	        std::string switch_y =str;
	        getline(ss,str,',');
	        auto node_index =std::stoi(str);
            if(LinkEnd(switch_x,switch_y)&&(node_index==1))
            {
                return;
            }
            else if(LinkEnd(switch_x,switch_y)&&(node_index==0))
            {
                continue;
            }
            else if(InvalidRow(switch_x,switch_y))
            {
                continue;
            }
            else
            {
                int x=FindSwitchIndex(switch_x);
                int y=FindSwitchIndex(switch_y);
                DoStatistic(x,y,node_index);
            }
        }
    }
    void UndigraphStatistic::SaveData(std::string filePath)//确定文件类型，文件格式。还是csv就可以
    {
        FileProcessing::SaveFile(*m_graph,filePath);
    }
    bool UndigraphStatistic::LinkEnd(std::string switch_x,std::string switch_y)
    {
        if(switch_x=="0"&&switch_y=="0")
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    bool UndigraphStatistic::InvalidRow(std::string switch_x,std::string switch_y)
    {
        if(switch_x==""||switch_y=="")
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void UndigraphStatistic::DoStatistic(int x,int y,int node_index)
    {
        m_graph->Statistic(x,y,node_index);
    }
    int UndigraphStatistic::FindSwitchIndex(std::string switch_id)
    {
        auto row=m_graph->GetSideLength();
        auto it = find(m_switch,m_switch+row,switch_id);
        auto index = it -m_switch;
        if(index == row)
        {
            return -1;//数据不在数组里面，查找失败
        }
        else
        {
            return index;    
        }
    }
    /*--------------------------------------------------------------------------------*/
    void FileProcessing::SetFilePath(std::string file)
    {
        m_filePath = file;
    }
    void FileProcessing::SaveFile(Graph graph,std::string filePath)
    {
//        m_filePath = filePath;
//        if(!m_filePath)
//        {
//            std::cerr<<"Please initialize the file path."<<std::endl;
//            return;
//        }
        std::ofstream out;
        out.open(filePath);
        if(out.is_open())
        {
	        auto length = graph.GetSideLength();
	        for(int i=0;i!=length;++i)
	        {
	            for(int j=0;j!=length;++j)
	            {
	                auto node = graph[i][j];
	                auto argLength = node.GetVSize();
	                for(int l=0;l!=argLength;++l)
	                {
                        if(l!=argLength-1)
                        {
	                        out<<node.GetArg(l)<<"_";
                        }
                        else
                        {
                            out<<node.GetArg(l);
                        }
	                }
                    out<<",";
	            }
                out<<"\n";
	        }
        }
        out.close();
     }
//     Graph FileProcessing::ReadFile(std::string file,int sideLength)
//     {
//	    std::ifstream in(file);
//	    std::string line;
//        static Graph graph(sideLength);
//	    while(getline(in,line))
//	    {
//            static int i=0;
//		    std::stringstream ss(line);
//		    std::string str;
//            for(int j=0;j!=sideLength;++j)
//            {
//		        getline(ss,str,',');
//                for(int size=0;size!=5;++size)
//                {
//                    auto pos=str.find('_');
//                    auto sub=str.substr(0,pos);
//                    str=str.substr(pos+1,str.size());
//                    graph[i][j].SetArg(size,std::stoi(sub));
//                }
//                    std::cout<<i<<'\t';
//            }
//            ++i;
//        }
//        return graph;
//     }
}
