#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/netanim-module.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("BottleNeckTcpScriptExample");

int
main (int argc, char *argv[])
{
  Time::SetResolution (Time::NS);
 // LogComponentEnable ("BottleNeckTcpScriptExample", LOG_LEVEL_INFO);
 // LogComponentEnable ("TcpL4Protocol", LOG_LEVEL_INFO);
 // LogComponentEnable ("PacketSink", LOG_LEVEL_ALL);
  //LogComponentEnable ("OnOff", LOG_LEVEL_ALL);


  NodeContainer nodes;
  nodes.Create (6);

  NodeContainer n0n1 = NodeContainer(nodes.Get(0),nodes.Get(1));
 // NodeContainer n1n2 = NodeContainer(nodes.Get(1),nodes.Get(2));
  NodeContainer n1n3 = NodeContainer(nodes.Get(1),nodes.Get(3));
 // NodeContainer n3n4 = NodeContainer(nodes.Get(3),nodes.Get(4));
  NodeContainer n3n5 = NodeContainer(nodes.Get(3),nodes.Get(5));

  InternetStackHelper internet;
  internet.Install(nodes);

  PointToPointHelper p2p;
  p2p.SetDeviceAttribute("DataRate",StringValue("100Mbps"));
  p2p.SetChannelAttribute("Delay",StringValue("2ms"));

  NetDeviceContainer nets;
  NetDeviceContainer d0d1 =p2p.Install(n0n1);
  //NetDeviceContainer d1d2 =p2p.Install(n1n2);
  NetDeviceContainer d1d3 =p2p.Install(n1n3);
  //NetDeviceContainer d3d4 =p2p.Install(n3n4);
  NetDeviceContainer d3d5 =p2p.Install(n3n5);

  Ipv4AddressHelper ipv4;
  ipv4.SetBase("10.1.1.0","255.255.255.0");
  Ipv4InterfaceContainer i0i1 = ipv4.Assign(d0d1);

  ipv4.SetBase("10.1.2.0","255.255.255.0");
  //ipv4.Assign(d1d2);

  ipv4.SetBase("10.1.3.0","255.255.255.0");
  Ipv4InterfaceContainer i1i3 = ipv4.Assign(d1d3);

  ipv4.SetBase("10.1.4.0","255.255.255.0");
  //ipv4.Assign(d3d4);

  ipv4.SetBase("10.1.5.0","255.255.255.0");
  ipv4.Assign(d3d5);
  Ipv4InterfaceContainer i3i5 = ipv4.Assign(d3d5);

  uint16_t port = 50000;
  ApplicationContainer sinkApp;
  Address sinkLocalAddress (InetSocketAddress (Ipv4Address::GetAny (), port));
  PacketSinkHelper sinkHelper ("ns3::TcpSocketFactory", sinkLocalAddress);
  sinkApp.Add(sinkHelper.Install(nodes));
  sinkApp.Start (Seconds (0.0));
  sinkApp.Stop (Seconds (10.0));

  OnOffHelper clientHelper ("ns3::TcpSocketFactory", Address ());
  clientHelper.SetAttribute ("OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=1]"));
  clientHelper.SetAttribute ("OffTime", StringValue ("ns3::ConstantRandomVariable[Constant=0]"));

  ApplicationContainer clientApps;
  AddressValue remoteAddress(InetSocketAddress (i0i1.GetAddress (1), port));
  clientHelper.SetAttribute("Remote",remoteAddress);
  clientApps.Add(clientHelper.Install(nodes.Get(0)));

  clientApps.Start(Seconds(1.0));
  clientApps.Stop (Seconds (10.0));


  //1-3应用创建

  OnOffHelper clientHelper2 ("ns3::TcpSocketFactory", Address ());
  clientHelper.SetAttribute ("OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=1]"));
  clientHelper.SetAttribute ("OffTime", StringValue ("ns3::ConstantRandomVariable[Constant=0]"));

  AddressValue remoteAddress3(InetSocketAddress (i1i3.GetAddress (1), port));
  clientHelper.SetAttribute("Remote",remoteAddress3);
  clientApps.Add(clientHelper.Install(nodes.Get(1)));

  clientApps.Start(Seconds(1.0));
  clientApps.Stop (Seconds (10.0));




  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
  //嗅探,记录所有节点相关的数据包

  AnimationInterface anim("TopologyOne.xml");

  Simulator::Run ();
  Simulator::Destroy ();
  return 0;
}
