/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/application-helper.h"
#include"ns3/resource-unit.h"
#include"ns3/resource-unit-container.h"
#include"ns3/resource-unit-helper.h"
#include"ns3/object.h"
#include <iostream>
#include"ns3/manager.h"
#include"ns3/manager-helper.h"
#include<iostream>
#include<string>
#include<fstream>
#include"ns3/stats-module.h"
#include<sstream>
// Default Network Topology
//
//       10.1.1.0
// n0 -------------- n1
//    point-to-point
//

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("FirstScriptExample");
void
TraceCpu (double oldValue, double newValue)
{
  std::cout<<"At time "<<Simulator::Now().As(Time::S) << " the old value of cpu  " << oldValue << " has changed to " << newValue << std::endl;
}
void
TraceMem (double oldValue, double newValue)
{
  std::cout <<"At time "<<Simulator::Now().As(Time::S)<< " the old value of memory  " << oldValue << " has changed to " << newValue << std::endl;
}
void
StringTraceNowId (std::string oldValue, std::string newValue)
{
  std::cout << "At time _s,the old value of NowId " << oldValue << " has changed to " << newValue << std::endl;
}
void
StringTraceNowStatus (std::string oldValue, std::string newValue)
{
  std::cout << "At time _s,the old status of task " << oldValue << " has changed to " << newValue << std::endl;
}

int
main (int argc, char *argv[])
{

  CommandLine cmd (__FILE__);
  cmd.Parse (argc, argv);

  Time::SetResolution (Time::NS);
  //LogComponentEnable ("Client", LOG_LEVEL_INFO);
  //LogComponentEnable ("Server", LOG_LEVEL_INFO);
  //LogComponentEnable ("DefaultGenerate", LOG_LEVEL_INFO);
  //LogComponentEnable ("DefaultSend", LOG_LEVEL_INFO);
  LogComponentEnable ("DefaultOrchestrator", LOG_LEVEL_INFO);
  //LogComponentEnable ("DefaultReceive", LOG_LEVEL_INFO);

  NodeContainer nodes;
  nodes.Create (2);

  PointToPointHelper pointToPoint;
  pointToPoint.SetDeviceAttribute ("DataRate", StringValue ("5Mbps"));
  pointToPoint.SetChannelAttribute ("Delay", StringValue ("2ms"));

  NetDeviceContainer devices;
  devices = pointToPoint.Install (nodes.Get(0),nodes.Get(1));

  InternetStackHelper stack;
  stack.Install (nodes);

  Ipv4AddressHelper address;
  address.SetBase ("10.1.1.0", "255.255.255.0");

  Ipv4InterfaceContainer interfaces = address.Assign (devices);

  ServerHelper echoServer (10);

  ApplicationContainer serverApps = echoServer.Install (nodes.Get (1));
  serverApps.Start (Seconds (0.0));
  serverApps.Stop (Seconds (100000));

  ClientHelper echoClient (interfaces.GetAddress (1), 10);

  ApplicationContainer clientApps_0 = echoClient.Install (nodes.Get (0));
 // ApplicationContainer clientApps_1 = echoClient.Install (nodes.Get (1));
  clientApps_0.Start (Seconds (0.0));
  clientApps_0.Stop (Seconds (100000));
//  clientApps_1.Start (Seconds (0.0));
//  clientApps_1.Start (Seconds (0.0));
    /* 向指定node安装资源部件*/
    ResourceHelper res;
    res.Install(nodes);
    Ptr<Node> node = nodes.Get(1);
    Ptr<ResourceContainer> container_2= node->GetObject<ResourceContainer>();
//	Ptr<Resource> re2 = CreateObject<Resource>();
//    container->Add(re2);
    Ptr<Resource> MyResource = *(container_2->Begin());
//    re2->TraceConnectWithoutContext ("RemainingMemory", MakeCallback (&TraceMem));
//    re2->TraceConnectWithoutContext ("RemainingCpu", MakeCallback (&TraceCpu));
    MyResource->TraceConnectWithoutContext ("RemainingMemory", MakeCallback (&TraceMem));
    MyResource->TraceConnectWithoutContext ("RemainingCpu", MakeCallback (&TraceCpu));
    MyResource->SetResourceId("2020001");
    MyResource->SetMemory(0.2493);
    MyResource->SetCpu(0.5);
    Ptr<Node> node_2 = nodes.Get(0);
    Ptr<ResourceContainer> container= node_2->GetObject<ResourceContainer>();
//	Ptr<Resource> re2 = CreateObject<Resource>();
//    container->Add(re2);
    Ptr<Resource> MyResource_2 = *(container->Begin());
//    re2->TraceConnectWithoutContext ("RemainingMemory", MakeCallback (&TraceMem));
//    re2->TraceConnectWithoutContext ("RemainingCpu", MakeCallback (&TraceCpu));
    MyResource_2->TraceConnectWithoutContext ("RemainingMemory", MakeCallback (&TraceMem));
    MyResource_2->TraceConnectWithoutContext ("RemainingCpu", MakeCallback (&TraceCpu));
    MyResource_2->SetResourceId("2020002");
    MyResource_2->SetMemory(0.2493);
    MyResource_2->SetCpu(0.5);
    /* 向指定node安装Manager*/
    ManagerHelper<Manager> manager;
    manager.Install(nodes);
    auto manager_0 = nodes.Get(0)->GetObject<Manager>();
    auto manager_1 = nodes.Get(1)->GetObject<Manager>();
    manager_0->SetMachineId("aaa");
    manager_1->SetMachineId("bbb");
    /* 构建machineId与IP地址的映射关系*/
    Address machine_1 = interfaces.GetAddress(1);
    Address machine_0 = interfaces.GetAddress(0);
    std::map<std::string,Address> map;
    map.insert(std::make_pair("aaa",machine_0));
    map.insert(std::make_pair("bbb",machine_1));
    manager_0->SetMap(map);
    //manager_1->SetMap(map);

    std::cout<<"--------------------------"<<std::endl;



  //  auto manager_1 = nodes.Get(1)->GetObject<Manager>();
    std::ifstream in("/home/ssf/tarballs/EasiEI/ns-3-dev/scratch/machine_14.csv");
    std::string line;
    getline(in,line);
    while(getline(in,line))
    {
        std::stringstream ss(line);
        std::string str;
        getline(ss,str,',');
        double time = std::stod(str)/1000000 ;
std::string        str_time =boost::lexical_cast<std::string>(time);
        getline(ss,str,',');
        std::string jobid =str;
        getline(ss,str,',');
        std::string job_stamp =str;
        std::string task_id = jobid+job_stamp;
        getline(ss,str,',');
        auto event =std::stod(str);
std::string        str_event =boost::lexical_cast<std::string>(event);
        getline(ss,str,',');
        auto prior =std::stod(str);
std::string        str_prior =boost::lexical_cast<std::string>(prior);
        getline(ss,str,',');
        double cpu =std::stod(str);
std::string        str_cpu =boost::lexical_cast<std::string>(cpu);
        getline(ss,str,',');
        double memory = std::stod(str);
std::string        str_memory =boost::lexical_cast<std::string>(memory);
        getline(ss,str,',');
        double end_time =std::stod(str)/1000000;
std::string        str_endTime =boost::lexical_cast<std::string>(end_time);
        manager_0->GetTaskInfo({task_id,str_cpu,str_memory,str_prior,"bbb",str_time,str_endTime  });

    }
    manager_0->Start();

    manager_0->ShowTaskStatus();
    Simulator::Schedule(Seconds(300),&Manager::ShowTaskStatus,manager_1);

//    std::initializer_list<std::string> l = {"s00001","2","2","3","aaa"};
  Names::Add ("/Names/Emitter",MyResource );

  //
  // This file helper will be used to put data values into a file.
  //

  // Create the file helper.
  FileHelper fileHelper;

  // Configure the file to be written.
  fileHelper.ConfigureFile ("mem-expamle2",
                            FileAggregator::FORMATTED);
  //fileHelper.ConfigureFile ("mem",
  //                          FileAggregator::FORMATTED);

  // Set the labels for this formatted output file.
//  fileHelper.Set2dFormat ("Time (Seconds) = %.3f\tCount = %lf");
  fileHelper.Set2dFormat ("%.3f,%lf");

  // Write the values generated by the probe.  The path that we
  // provide helps to disambiguate the source of the trace.
  fileHelper.WriteProbe ("ns3::DoubleProbe",
                         "/Names/Emitter/RemainingMemory",
                         "Output");

    Simulator::Stop(Seconds(100000));
  Simulator::Run ();
//    Simulator::Schedule(Seconds(3.0),&Manager::GetTaskInfo,no_manager,{"s00001","2","2","3","aaa"});
  Simulator::Destroy ();
  return 0;
}
